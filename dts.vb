﻿Imports System.IO
Public Class dts
    'Public dts As New DataSet
    Public state As Boolean = False
    Private file_path As String
    Private files As FileStream
    Public dtt As DataTable
    Private sr As StreamReader
    Public Sub New(ByVal path As String)
        Me.file_path = path
    End Sub
    Public Sub Charge_dataset()
        Try
            Dim line As String
            Dim count_columns As Integer
            Dim row_number As Integer = 0
            'files = File.OpenRead(file_path)
            sr = New StreamReader(file_path)
            count_columns = charge_collumns_name(sr.ReadLine)
            If count_columns = 0 Then Exit Sub
            line = sr.ReadLine
            Do While Not line Is Nothing
                Dim column() As String = line.Split(vbTab)
                dtt.Rows.Add()
                For i = 0 To count_columns - 1
                    If i <= column.Length - 1 Then
                        dtt.Rows(row_number).Item(dtt.Columns(i).ColumnName) = column(i)
                    End If
                Next
                line = sr.ReadLine
                row_number = row_number + 1
            Loop
            state = True
        Catch ex As Exception
            MsgBox(ex.Message)
            state = False
        End Try
    End Sub
    Public Function charge_collumns_name(ByVal line As String) As Integer
        charge_collumns_name = 0
        Dim campos As String() = line.Split(vbTab)
        dtt = New DataTable()
        For i = 0 To campos.Length - 1
            dtt.Columns.Add(campos(i), GetType(String))
        Next
        charge_collumns_name = campos.Length
    End Function
End Class
