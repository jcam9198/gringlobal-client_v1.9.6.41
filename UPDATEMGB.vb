﻿Option Explicit On
Imports System.Data.SqlClient
Public Class UPDATEMGB
    Public Sub enabled_buttons(ByVal uno As Boolean, ByVal dos As Boolean, ByVal tres As Boolean, ByVal cuatro As Boolean, ByVal cinco As Boolean, ByVal seis As Boolean, ByVal siete As Boolean)
        BTN_COOP.Enabled = uno
        'BTN_COOPMGB.Enabled = dos
        BTN_INSTITUTION.Enabled = tres
        BTN_SEEDENT.Enabled = cuatro
        BTN_SEEDSHIP.Enabled = cinco
        BTN_REG.Enabled = seis
        btn_passport.Enabled = siete
    End Sub
    Private Sub BTN_COOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_COOP.Click
        que_tiene_cargado = "COOP_COOPMGB"
        Dim num_ar As Integer
        path_err = path_aplicacion & que_tiene_cargado & ".txt"
        num_ar = FreeFile()
        FileOpen(num_ar, path_err, OpenMode.Output)
        FileClose(num_ar)
        If Not conecta_a_excel() Then Exit Sub
        If Not carga_encabezado_bds(que_tiene_cargado) Then Exit Sub
        If Not compara_regeneracion() Then Exit Sub
        DataGridView1.DataSource = dts_excel.Tables(0)
        Call enabled_buttons(False, False, False, False, False, False, False)
        If Not COMPARA_DATOS_COOP() Then
            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True
    End Sub

    Private Sub UPDATEMGB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        sigue = 0
        btn_clean.Enabled = False
        btn_savetodbs.Enabled = False
        btn_view_errors.Visible = False
        dtg_list.Visible = False
        apaga_labels()
    End Sub
    Public Sub pone_visible_label(ByVal CUAL As Byte)
        Select Case CUAL
            Case 1
                lbl_null.Visible = True
            Case 2
                lbl_incompatible.Visible = True
            Case 3
                lbl_exist.Visible = True
            Case 4
                lbl_invalid.Visible = True
            Case 5
                lbl_excess.Visible = True
            Case 6
                lbl_related.Visible = True
            Case 7
                lbl_blank.Visible = True
            Case 8
                lbl_possible.Visible = True
            Case 9
                lbl_existe_en_excel.Visible = True
        End Select
        dtg_list.Visible = True
    End Sub
    Public Sub apaga_labels()
        lbl_null.Visible = False
        lbl_incompatible.Visible = False
        lbl_exist.Visible = False
        lbl_invalid.Visible = False
        lbl_excess.Visible = False
        lbl_related.Visible = False
        lbl_blank.Visible = False
        lbl_possible.Visible = False
        lbl_existe_en_excel.Visible = False
    End Sub


    Private Sub BTN_INSTITUTION_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_INSTITUTION.Click
        que_tiene_cargado = "INSTITUTION"
        If Not conecta_a_excel() Then Exit Sub
        Call enabled_buttons(False, False, True, False, False, False, False)
        If Not VALIDA_DATOS(3) Then
            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True
    End Sub

    Private Sub BTN_SEEDENT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_SEEDENT.Click
        que_tiene_cargado = "SEEDENT"
        If Not conecta_a_excel() Then Exit Sub
        Call enabled_buttons(False, False, False, True, False, False, False)
        If Not VALIDA_DATOS(4) Then
            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True

    End Sub

    Private Sub BTN_SEEDSHIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_SEEDSHIP.Click
        que_tiene_cargado = "SEEDSHIP"
        If Not conecta_a_excel() Then Exit Sub
        Call enabled_buttons(False, False, False, False, False, False, False)
        If Not VALIDA_DATOS(5) Then

            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True

    End Sub
    Private Sub btn_passport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_passport.Click
        que_tiene_cargado = "PASSPORT"
        path_err = path_aplicacion & que_tiene_cargado & "_err.txt"
        Dim NUM_AR As Integer
        NUM_AR = FreeFile()
        FileOpen(NUM_AR, path_err, OpenMode.Output)
        FileClose(NUM_AR)
        If Not conecta_a_excel() Then Exit Sub

        'If Not carga_encabezado_bds(que_tiene_cargado) Then Exit Sub
        'If Not compara_regeneracion() Then Exit Sub
        DataGridView1.DataSource = dts_excel.Tables(0)
        Call enabled_buttons(False, False, False, False, False, False, True)
        If Not compara("PASSPORT") Then
            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_REG.Click
        que_tiene_cargado = "REGEN"
        Dim num_ar As Integer
        path_err = path_aplicacion & que_tiene_cargado & ".txt"
        num_ar = FreeFile()
        FileOpen(num_ar, path_err, OpenMode.Output)
        FileClose(num_ar)
        If Not conecta_a_excel() Then Exit Sub
        If Not carga_encabezado_bds(que_tiene_cargado) Then Exit Sub
        If Not compara_regeneracion() Then Exit Sub
        DataGridView1.DataSource = dts_excel.Tables(0)
        Call enabled_buttons(False, False, False, False, False, False, False)
        If Not compara_datos_regeneracion() Then
            btn_savetodbs.Enabled = False
            btn_view_errors.Visible = True
        Else
            btn_savetodbs.Enabled = True
            btn_view_errors.Visible = False
        End If
        btn_clean.Enabled = True

    End Sub


    Private Sub btn_savetodbs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_savetodbs.Click
        If que_tiene_cargado = "REGEN" Then
            tran = cnx.BeginTransaction
            If Not guarda_regeneracion() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not guarda_actbascollection() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not crea_excel_regen() Then
                tran.Rollback()
                MsgBox("No se pudo crear el Excel que da el BankAccessionNumber")
                Exit Sub
            End If
            tran.Commit()
        ElseIf que_tiene_cargado = "PASSPORT" Then
            tran = cnx.BeginTransaction
            If Not guarda_accid() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not guarda_passport() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not guarda_collection() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not crea_excel_passport() Then
                tran.Rollback()
                MsgBox("No se pudo crear el Excel que da el Accid")
                Exit Sub
            End If
            tran.Commit()
        ElseIf que_tiene_cargado = "COOP_COOPMGB" Then
            tran = cnx.BeginTransaction
            If Not GUARDA_EN_COOP() Then
                tran.Rollback()
                Exit Sub
            End If
            If Not GUARDA_EN_COOPMGB() Then
                tran.Rollback()
                Exit Sub
            End If
            tran.Commit()
        Else
            If Not SAVE_TO_DATABASE() Then Exit Sub
        End If
        PGRBAR.Visible = False
        limpia_todo()
        Call enabled_buttons(True, True, True, True, True, True, True)
        btn_savetodbs.Enabled = False
        btn_clean.Enabled = False
        btn_view_errors.Visible = False
        apaga_labels()
        dtg_list.Visible = False
    End Sub

    Private Sub btn_clean_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_clean.Click

        limpia_todo()
        Call enabled_buttons(True, True, True, True, True, True, True)
        btn_savetodbs.Enabled = False
        btn_clean.Enabled = False
        btn_view_errors.Visible = False
        apaga_labels()
        dtg_list.Visible = False
    End Sub


    Private Sub dtg_list_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtg_list.CellClick
        Dim row As Integer
        Dim col As Integer
        If e.RowIndex < 0 Then Exit Sub
        col = dtg_list(0, e.RowIndex).Value.ToString - 1
        row = dtg_list(1, e.RowIndex).Value.ToString - 1
        DataGridView1.CurrentCell = DataGridView1(col, row)

    End Sub


    Private Sub btn_view_errors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_view_errors.Click
        If IO.File.Exists(path_err) Then
            Process.Start("notepad.exe", path_err)
        End If
    End Sub

    Private Sub BTN_EXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_EXIT.Click
        Me.Close()
    End Sub

End Class