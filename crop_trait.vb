﻿Public Class crop_trait
    Public row As Integer
    Private code_name As String
    Private crop_trait_id As Integer
    Private data_type_code As String
    Private max_lenght As Integer = 0
    Private numeric_maximun As Double = 0
    Private numeric_minimun As Double = 0
    Private compound_rank_crop_trait As String
    Private compound_link_crop_trait As String
    Private update As Boolean = False
    Private mistake As Boolean = True
    Private code_value_crop_trait As String


    Public crop_trait_name As String
    Public crop_type As String
    Public inventory_id As Integer
    Public value As String
    Public method_name As String
    Private method_id As Integer
    Public user_id As Integer



    Public Sub New(ByVal _code_name As String, ByVal _value As String, ByVal _method_name As String, ByVal _row As Integer, ByVal _compound As String, ByVal _code_value_crop_trait As String)
        code_name = _code_name
        value = _value
        method_name = _method_name
        row = _row
        compound_rank_crop_trait = _compound
        code_value_crop_trait = _code_value_crop_trait
    End Sub

    Public Function load_crop_trait() As Boolean
        load_crop_trait = False
        Dim DTT As New DataTable
        DTT = carga_data_reader("SELECT * FROM crop_trait WHERE coded_name = '" & code_name & "'", "GRINGLOBAL")
        If DTT.Rows.Count = 0 Then
            load_crop_trait = False
            Exit Function
        End If
        With DTT.Rows(0)
            crop_trait_id = .Item("crop_trait_id").ToString
            data_type_code = .Item("data_type_code").ToString
            If .Item("max_length").ToString <> "" Then max_lenght = .Item("max_length").ToString
            If .Item("numeric_maximum").ToString <> "" Then numeric_maximun = .Item("numeric_maximum").ToString
            If .Item("numeric_minimum").ToString <> "" Then numeric_minimun = .Item("numeric_minimum").ToString
        End With
        load_crop_trait = True
    End Function
    Public Function check_crop_trait() As Integer
        check_crop_trait = 0
        If load_crop_trait() = False Then
            check_crop_trait = 13
            Exit Function
        End If
        If data_type_code = "NUMERIC" Then
            If Not IsNumeric(value) Then
                check_crop_trait = 2
                Exit Function
            ElseIf (numeric_minimun <> 0 And value < numeric_minimun) Or (numeric_maximun <> 0 And value > numeric_maximun) Then
                check_crop_trait = 11
                Exit Function
            End If
        ElseIf data_type_code = "CHAR" Then

            If compound_rank_crop_trait <> "" Then compound_link_crop_trait = make_compound("select Encabezado, compound_link_crop_trait as link from CONFIGURATION_PASSPORT where crop_crait_id = '" & code_name & "' and compound_rank_crop_trait  is not null order by compound_rank_crop_trait", row)
            If compound_link_crop_trait <> "" Then value = compound_link_crop_trait
            If Len(value) > max_lenght Then
                check_crop_trait = 5
                Exit Function
            End If
        End If
        If code_value_crop_trait <> "" Then
            check_crop_trait = verifica_code_value(value, code_value_crop_trait)
        End If
        If check_crop_trait = 0 Then
            mistake = True
        Else
            mistake = False
        End If
    End Function

    Public Function return_crop_id() As Boolean
        return_crop_id = False
        Me.crop_trait_id = return_integer("select top 1 crop_trait_id from crop_trait where coded_name = '" & Me.crop_trait_name & "'", "gringlobal")
        If Me.crop_trait_id > 0 Then return_crop_id = True
    End Function
    Public Function return_method_id() As Boolean
        return_method_id = False
        Me.method_id = return_integer("select TOP 1 method_id from method where name = '" & Me.method_name & "'", "gringlobal")
        If Me.method_id > 0 Then return_method_id = True
    End Function
    Public Function save_crop_trait() As Boolean
        save_crop_trait = False
        If return_crop_id() = False Then Exit Function
        If return_method_id() - False Then Exit Function
        If return_integer("select COUNT(*) from crop_trait_observation where inventory_id = " & Me.inventory_id & " and crop_trait_id = " & crop_trait_id & " and method_id = " & Me.method_id, "gringlobal") > 0 Then
            save_crop_trait = update_crop()
        Else
            save_crop_trait = insert_crop()
        End If
    End Function
    Public Function update_crop() As Boolean
        update_crop = False
        If Me.crop_type = "numeric_value" Then
            update_crop = ejecuta_query("update crop_trait_observation set numeric_value = '" & Me.value & "' WHERE inventory_id = " & Me.inventory_id & " and crop_trait_id = " & Me.crop_trait_id & " and method_id = " & Me.method_id, "gringlobal")
        ElseIf Me.crop_type = "string_value" Then
            update_crop = ejecuta_query("update crop_trait_observation set string_value = '" & Me.value & "' WHERE inventory_id = " & Me.inventory_id & " and crop_trait_id = " & Me.crop_trait_id & " and method_id = " & Me.method_id, "gringlobal")
        End If
    End Function
    Public Function insert_crop() As Boolean
        insert_crop = False
        Dim str As String
        If Me.crop_type = "numeric_value" Then
            str = "insert into crop_trait_observation " & _
                  "(inventory_id,crop_trait_id,numeric_value,method_id,is_archived,created_date,created_by,owned_date,owned_by) " & _
                  "values(" & Me.inventory_id & "," & Me.crop_trait_id & "," & Me.value & "," & Me.method_id & ",N,GETDATE()," & Me.user_id & ",GETDATE()," & Me.user_id & ")"
        ElseIf Me.crop_type = "string_value" Then
            str = "insert into crop_trait_observation " & _
                  "(inventory_id,crop_trait_id,string_value,method_id,is_archived,created_date,created_by,owned_date,owned_by) " & _
                  "values(" & Me.inventory_id & "," & Me.crop_trait_id & "," & Me.value & "," & Me.method_id & ",N,GETDATE()," & Me.user_id & ",GETDATE()," & Me.user_id & ")"
        End If
        insert_crop = ejecuta_query(str, "gringlobal")
    End Function

End Class
