﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UPDATEMGB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BTN_COOP = New System.Windows.Forms.Button
        Me.BTN_INSTITUTION = New System.Windows.Forms.Button
        Me.BTN_SEEDENT = New System.Windows.Forms.Button
        Me.BTN_SEEDSHIP = New System.Windows.Forms.Button
        Me.BTN_REG = New System.Windows.Forms.Button
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.btn_savetodbs = New System.Windows.Forms.Button
        Me.btn_clean = New System.Windows.Forms.Button
        Me.dtg_list = New System.Windows.Forms.DataGridView
        Me.Col = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Row = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btn_view_errors = New System.Windows.Forms.Button
        Me.btn_passport = New System.Windows.Forms.Button
        Me.BTN_EXIT = New System.Windows.Forms.Button
        Me.PGRBAR = New System.Windows.Forms.ProgressBar
        Me.p_error = New System.Windows.Forms.Panel
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtg_list, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BTN_COOP
        '
        Me.BTN_COOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_COOP.Location = New System.Drawing.Point(155, 8)
        Me.BTN_COOP.Name = "BTN_COOP"
        Me.BTN_COOP.Size = New System.Drawing.Size(139, 33)
        Me.BTN_COOP.TabIndex = 0
        Me.BTN_COOP.Text = "COOP COOPMGB"
        Me.BTN_COOP.UseVisualStyleBackColor = True
        '
        'BTN_INSTITUTION
        '
        Me.BTN_INSTITUTION.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_INSTITUTION.Location = New System.Drawing.Point(12, 8)
        Me.BTN_INSTITUTION.Name = "BTN_INSTITUTION"
        Me.BTN_INSTITUTION.Size = New System.Drawing.Size(126, 33)
        Me.BTN_INSTITUTION.TabIndex = 2
        Me.BTN_INSTITUTION.Text = "INSTITUTION"
        Me.BTN_INSTITUTION.UseVisualStyleBackColor = True
        '
        'BTN_SEEDENT
        '
        Me.BTN_SEEDENT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_SEEDENT.Location = New System.Drawing.Point(463, 8)
        Me.BTN_SEEDENT.Name = "BTN_SEEDENT"
        Me.BTN_SEEDENT.Size = New System.Drawing.Size(126, 33)
        Me.BTN_SEEDENT.TabIndex = 3
        Me.BTN_SEEDENT.Text = "SEEDENT"
        Me.BTN_SEEDENT.UseVisualStyleBackColor = True
        '
        'BTN_SEEDSHIP
        '
        Me.BTN_SEEDSHIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_SEEDSHIP.Location = New System.Drawing.Point(314, 8)
        Me.BTN_SEEDSHIP.Name = "BTN_SEEDSHIP"
        Me.BTN_SEEDSHIP.Size = New System.Drawing.Size(126, 33)
        Me.BTN_SEEDSHIP.TabIndex = 4
        Me.BTN_SEEDSHIP.Text = "SEEDSHIP"
        Me.BTN_SEEDSHIP.UseVisualStyleBackColor = True
        '
        'BTN_REG
        '
        Me.BTN_REG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_REG.Location = New System.Drawing.Point(612, 8)
        Me.BTN_REG.Name = "BTN_REG"
        Me.BTN_REG.Size = New System.Drawing.Size(136, 33)
        Me.BTN_REG.TabIndex = 5
        Me.BTN_REG.Text = "REGENERATION"
        Me.BTN_REG.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataGridView1.Location = New System.Drawing.Point(12, 47)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(789, 519)
        Me.DataGridView1.TabIndex = 6
        '
        'btn_savetodbs
        '
        Me.btn_savetodbs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_savetodbs.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_savetodbs.Location = New System.Drawing.Point(143, 615)
        Me.btn_savetodbs.Name = "btn_savetodbs"
        Me.btn_savetodbs.Size = New System.Drawing.Size(139, 28)
        Me.btn_savetodbs.TabIndex = 10
        Me.btn_savetodbs.Text = "SAVE TO DBs"
        Me.btn_savetodbs.UseVisualStyleBackColor = True
        '
        'btn_clean
        '
        Me.btn_clean.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_clean.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_clean.Location = New System.Drawing.Point(336, 615)
        Me.btn_clean.Name = "btn_clean"
        Me.btn_clean.Size = New System.Drawing.Size(139, 28)
        Me.btn_clean.TabIndex = 11
        Me.btn_clean.Text = "CLEAR"
        Me.btn_clean.UseVisualStyleBackColor = True
        '
        'dtg_list
        '
        Me.dtg_list.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtg_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtg_list.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Col, Me.Row})
        Me.dtg_list.Location = New System.Drawing.Point(828, 345)
        Me.dtg_list.Name = "dtg_list"
        Me.dtg_list.Size = New System.Drawing.Size(145, 221)
        Me.dtg_list.TabIndex = 13
        '
        'Col
        '
        Me.Col.HeaderText = "Col"
        Me.Col.Name = "Col"
        Me.Col.ReadOnly = True
        Me.Col.Width = 50
        '
        'Row
        '
        Me.Row.HeaderText = "Row"
        Me.Row.Name = "Row"
        Me.Row.ReadOnly = True
        Me.Row.Width = 50
        '
        'btn_view_errors
        '
        Me.btn_view_errors.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_view_errors.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_view_errors.Location = New System.Drawing.Point(1004, 345)
        Me.btn_view_errors.Name = "btn_view_errors"
        Me.btn_view_errors.Size = New System.Drawing.Size(70, 71)
        Me.btn_view_errors.TabIndex = 16
        Me.btn_view_errors.Text = "View Errors"
        Me.btn_view_errors.UseVisualStyleBackColor = True
        '
        'btn_passport
        '
        Me.btn_passport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_passport.Location = New System.Drawing.Point(766, 8)
        Me.btn_passport.Name = "btn_passport"
        Me.btn_passport.Size = New System.Drawing.Size(136, 33)
        Me.btn_passport.TabIndex = 17
        Me.btn_passport.Text = "PASSSPORT"
        Me.btn_passport.UseVisualStyleBackColor = True
        '
        'BTN_EXIT
        '
        Me.BTN_EXIT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTN_EXIT.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_EXIT.Location = New System.Drawing.Point(539, 615)
        Me.BTN_EXIT.Name = "BTN_EXIT"
        Me.BTN_EXIT.Size = New System.Drawing.Size(139, 28)
        Me.BTN_EXIT.TabIndex = 20
        Me.BTN_EXIT.Text = "EXIT"
        Me.BTN_EXIT.UseVisualStyleBackColor = True
        '
        'PGRBAR
        '
        Me.PGRBAR.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PGRBAR.Location = New System.Drawing.Point(12, 576)
        Me.PGRBAR.Name = "PGRBAR"
        Me.PGRBAR.Size = New System.Drawing.Size(788, 20)
        Me.PGRBAR.TabIndex = 21
        Me.PGRBAR.Visible = False
        '
        'p_error
        '
        Me.p_error.Location = New System.Drawing.Point(828, 51)
        Me.p_error.Name = "p_error"
        Me.p_error.Size = New System.Drawing.Size(258, 288)
        Me.p_error.TabIndex = 22
        '
        'UPDATEMGB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1098, 655)
        Me.Controls.Add(Me.p_error)
        Me.Controls.Add(Me.PGRBAR)
        Me.Controls.Add(Me.BTN_EXIT)
        Me.Controls.Add(Me.btn_passport)
        Me.Controls.Add(Me.btn_view_errors)
        Me.Controls.Add(Me.dtg_list)
        Me.Controls.Add(Me.btn_clean)
        Me.Controls.Add(Me.btn_savetodbs)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.BTN_REG)
        Me.Controls.Add(Me.BTN_SEEDSHIP)
        Me.Controls.Add(Me.BTN_SEEDENT)
        Me.Controls.Add(Me.BTN_INSTITUTION)
        Me.Controls.Add(Me.BTN_COOP)
        Me.Name = "UPDATEMGB"
        Me.Text = "UPDATEMGB"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtg_list, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BTN_COOP As System.Windows.Forms.Button
    Friend WithEvents BTN_INSTITUTION As System.Windows.Forms.Button
    Friend WithEvents BTN_SEEDENT As System.Windows.Forms.Button
    Friend WithEvents BTN_SEEDSHIP As System.Windows.Forms.Button
    Friend WithEvents BTN_REG As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btn_savetodbs As System.Windows.Forms.Button
    Friend WithEvents btn_clean As System.Windows.Forms.Button
    Friend WithEvents dtg_list As System.Windows.Forms.DataGridView
    Friend WithEvents Col As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Row As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_view_errors As System.Windows.Forms.Button
    Friend WithEvents btn_passport As System.Windows.Forms.Button
    Friend WithEvents BTN_EXIT As System.Windows.Forms.Button
    Friend WithEvents PGRBAR As System.Windows.Forms.ProgressBar
    Friend WithEvents p_error As System.Windows.Forms.Panel
End Class
