﻿Option Explicit On
Imports System.IO

Public Class CONECTION
    Public servidor() As String
    Public usuario() As String
    Public path_list_server As String

    Private Sub btn_conect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_conect.Click
        If Me.txt_user.Text = "" Or Me.txt_pwd.Text = "" Then Exit Sub
        If cmb_server.Text = "" Then
            MsgBox("Select a server")
            Exit Sub
        End If
        carga_valores_coneccion()
    End Sub
    Public Sub carga_valores_coneccion()
        name_servidor = cmb_server.Text
        user = Me.txt_user.Text
        pwd = Me.txt_pwd.Text
        If conection_to_dbs() Then
            guarda_valores_server_user(path_list_server, name_servidor, user)
            sigue = 1
            Me.Close()
        Else
            MsgBox("Username or Password incorrect")
            Me.txt_user.Text = ""
            Me.txt_pwd.Text = ""
        End If
    End Sub
    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        sigue = 0
        Me.Close()
    End Sub

    Private Sub txt_pwd_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_pwd.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Return) Then
            If Me.txt_user.Text = "" Or Me.txt_pwd.Text = "" Then Exit Sub
            If cmb_server.Text = "" Then
                MsgBox("Select a server")
                Exit Sub
            End If
            carga_valores_coneccion()
        End If
    End Sub

    Private Sub CONECTION_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        path_aplicacion = CurDir()
        path_aplicacion = path_aplicacion & "\"
        sigue = 0
        name_servidor = ""
        user = ""
        pwd = ""
        path_list_server = path_aplicacion & "list_server.txt"
        If IO.File.Exists(path_list_server) Then
            Dim cua As Integer
            cua = lee_archivo_list_server(path_list_server)
            If cua > 0 Then carga_combo_servidor(cua)
            cmb_server.SelectedIndex = 0
            txt_user.Text = carga_servidor.usuario(0)
        Else
            txt_user.Text = ""
        End If
        txt_pwd.Focus()

    End Sub
    Public Sub carga_combo_servidor(ByVal cuan As Integer)
        Dim i As Integer
        Dim listaServidores As List(Of String) = New List(Of String)
        For i = 0 To cuan
            listaServidores.Add(carga_servidor.servidor(i))
        Next
        cmb_server.DataSource = listaServidores
    End Sub
    Public Sub limpia_user_ser()
        If Len(name_servidor) > 2 Then
            name_servidor = Mid(name_servidor, 2, Len(name_servidor))
            name_servidor = Mid(name_servidor, 1, Len(name_servidor) - 1)
        End If
        If Len(user) > 2 Then
            user = Mid(user, 2, Len(user))
            user = Mid(user, 1, Len(user) - 1)
        End If

    End Sub

   
End Class