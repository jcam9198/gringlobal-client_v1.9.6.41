﻿Public Class accession_inv_name
    Private value As String
    Private acc_inv_name As String
    Private rank As Integer
    Private code_value_acc_inv_name As String
    Private compound_rank_acc_inv_name As String
    Private compound_link_acc_inv_name As String
    Private row As Integer

    Public Sub New(ByVal _value As String, _
                   ByVal _acc_inv_name As String, _
                   ByVal _code_value_acc_inv_name As String, _
                   ByVal _compound_rank_acc_inv_name As String, _
                   ByVal _row As Integer)
        value = _value
        acc_inv_name = _acc_inv_name
        code_value_acc_inv_name = _code_value_acc_inv_name
        compound_rank_acc_inv_name = _compound_rank_acc_inv_name
        row = _row
    End Sub

    Public Function check_accession_inv_name() As Integer
        check_accession_inv_name = 0
        Dim DTT As New DataTable
        DTT = carga_data_reader("select IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'accession_inv_name' and COLUMN_NAME = 'plant_name'", "GRINGLOBAL")
        If DTT.Rows.Count = 0 Then
            check_accession_inv_name = 14
            Exit Function
        End If
        With DTT.Rows(0)
            If compound_rank_acc_inv_name <> "" Then compound_link_acc_inv_name = make_compound("select Encabezado, compound_link_accession_inv_name as link from CONFIGURATION_PASSPORT where field = '" & acc_inv_name & "' and compound_rank_accession_inv_name is not null order by compound_rank_accession_inv_name", row)
            If compound_link_acc_inv_name <> "" Then value = compound_link_acc_inv_name
            If Len(value) > .Item("CHARACTER_MAXIMUM_LENGTH").ToString Then
                check_accession_inv_name = 15
                Exit Function
            End If
            check_accession_inv_name = verifica_code_value(value, code_value_acc_inv_name)
        End With
    End Function
End Class
