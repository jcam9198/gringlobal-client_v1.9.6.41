﻿Public Class table_field

    Private value As String
    Private table As String
    Private field As String
    Private code_value_field As String
    Private compound_rank As String
    Private compound_link As String
    Private row As Integer
    Private is_alt As Boolean = False


    Public Sub New(ByVal _value As String, ByVal _table As String, ByVal _field As String, ByVal _code_value_field As String, ByVal _compound_rank As String, ByVal _row As Integer, ByVal _alt As Boolean)
        value = _value
        table = _table
        field = _field
        code_value_field = _code_value_field
        compound_rank = _compound_rank
        row = _row
        is_alt = _alt
    End Sub

    Public Function check_table_field() As Integer
        check_table_field = 0
        Dim DTT As New DataTable
        Dim val As String
        DTT = carga_data_reader("select IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '" & table & "' and COLUMN_NAME = '" & field & "'", "GRINGLOBAL")
        If DTT.Rows.Count = 0 Then
            check_table_field = 14
            Exit Function
        End If
        With DTT.Rows(0)
            If .Item("data_type").ToString = "int" Or .Item("data_type").ToString = "decimal" Then
                If Not IsNumeric(value) Then
                    check_table_field = 2
                    Exit Function
                End If
            ElseIf .Item("data_type").ToString = "nvarchar" Then
                If compound_rank <> "" Then
                    If is_alt Then
                        compound_link = make_compound("select Encabezado, compound_link_alt as link from CONFIGURATION_PASSPORT where field_alt = '" & field & "' and compound_rank_alt is not null order by compound_rank_alt ", row)
                    Else
                        compound_link = make_compound("select Encabezado, compound_link as link from CONFIGURATION_PASSPORT where field = '" & field & "' and compound_rank is not null order by compound_rank ", row)
                    End If
                    If compound_link <> "" Then value = compound_link
                End If
                If .Item("CHARACTER_MAXIMUM_LENGTH").ToString <> -1 And Len(value) > .Item("CHARACTER_MAXIMUM_LENGTH").ToString Then
                    check_table_field = 15
                    Exit Function
                End If
            End If
            If code_value_field <> "" Then
                check_table_field = verifica_code_value(value, code_value_field)
            End If
        End With
    End Function
    

End Class
