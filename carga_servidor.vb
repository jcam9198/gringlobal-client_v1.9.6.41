﻿Option Explicit On
Imports System.IO
Module carga_servidor
    Public servidor() As String
    Public usuario() As String
    Public serv() As String
    Public usu() As String
    Public Function lee_archivo_list_server(ByVal path As String) As Integer
        Dim fs As New StreamReader(path)
        Dim linea As String
        Dim cuantos As Integer
        linea = fs.ReadLine
        cuantos = 1
        Do While Not linea Is Nothing
            Dim cad() As String = linea.Split(",")
            ReDim Preserve servidor(cuantos)
            ReDim Preserve usuario(cuantos)
            servidor(cuantos - 1) = cad(0)
            usuario(cuantos - 1) = cad(1)
            cuantos = cuantos + 1
            linea = fs.ReadLine
        Loop
        fs.Close()
        lee_archivo_list_server = cuantos - 1

    End Function

    Public Sub guarda_valores_server_user(ByVal path As String, ByVal ser As String, ByVal user As String)
        Dim fs As New StreamReader(path)
        Dim linea As String
        Dim cuantos As Integer
        linea = fs.ReadLine
        cuantos = 1
        Do While Not linea Is Nothing
            Dim cad() As String = linea.Split(",")
            ReDim Preserve serv(cuantos)
            ReDim Preserve usu(cuantos)
            serv(cuantos - 1) = cad(0)
            usu(cuantos - 1) = cad(1)
            cuantos = cuantos + 1
            linea = fs.ReadLine
        Loop
        fs.Close()
        Dim wri As StreamWriter
        Dim fi As FileStream
        fi = File.Create(path)
        wri = New StreamWriter(fi)
        wri.WriteLine(ser & "," & user)
        Dim i As Integer
        For i = 0 To cuantos - 2
            If serv(i) <> ser Then wri.WriteLine(serv(i) & "," & usu(i))
        Next
        wri.Close()
        fi.Close()
    End Sub

End Module
