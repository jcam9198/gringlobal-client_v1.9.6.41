﻿Module tools
    Public arrays(,) As record

    Public Function compara(ByVal de_cual As String) As Boolean
        compara = True

        Dim i As Integer
        Dim j As Integer
        Dim columna_name As String
        Dim valor As String
        Dim err As Integer
        ReDim arrays(dts_excel.Tables(0).Rows.Count - 1, dts_excel.Tables(0).Columns.Count - 1)
        For i = 0 To dts_excel.Tables(0).Rows.Count - 1
            With dts_excel.Tables(0).Rows(i)
                For j = 0 To dts_excel.Tables(0).Columns.Count - 1
                    columna_name = dts_excel.Tables(0).Columns(j).ColumnName.ToString
                    valor = dts_excel.Tables(0).Rows(i).Item(columna_name).ToString
                    Dim new_record As New record(columna_name, valor, i, de_cual)
                    new_record.record_checks()
                    err = new_record.get_mistake
                    If err > 0 Then
                        MARCA_ERROR_dataview(err, j, i)
                        compara = False
                    End If
                    arrays(i, j) = new_record
                Next
            End With
        Next
    End Function
    Public Function make_compound(ByVal query As String, ByVal row As Integer) As String
        make_compound = ""
        Dim dtt As New DataTable
        Dim str As String = ""
        dtt = carga_data_reader(query, "configuration")
        If dtt.Rows.Count > 0 Then
            For i = 0 To dtt.Rows.Count - 1
                str = str & dts_excel.Tables(0).Rows(row).Item(dtt.Rows(i).Item("Encabezado").ToString).ToString
                If i < dtt.Rows.Count - 1 Then
                    str = str & dtt.Rows(i).Item("link").ToString
                End If
            Next
        End If
        make_compound = str
    End Function
    Public Function verifica_code_value(ByVal value As String, ByVal code_value As String) As Integer
        verifica_code_value = 0
        If code_value = "date" Then
            If Not IsDate(value) Then
                verifica_code_value = 4
            End If
        ElseIf return_integer("select COUNT(*) from code_value cv inner join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = '" & code_value & "' and (cv.value = '" & value & "' or cvl.title = '" & value & "')", "gringlobal") = 0 Then
            verifica_code_value = 12
            Exit Function
        End If
    End Function
    Public Sub mark_mistake()

    End Sub
    Public Sub MARCA_ERROR_dataview(ByVal TIPO As Byte, ByVal COL As Integer, ByVal ROW As Integer)
        Dim num As Integer
        Dim str_err As String
        With UPDATEMGB
            .dtg_list.Rows.Add(COL + 1, ROW + 1)

            Select Case TIPO
                Case 1
                    .DataGridView1(COL, ROW).Style.BackColor = Color.Red
                    .lbl_null.Text = "This value can not be null"
                    str_err = "Error in Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " the field can't be Null"
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Red
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Red
                Case 2
                    .DataGridView1(COL, ROW).Style.BackColor = Color.Yellow
                    .lbl_incompatible.Text = "Incompatible value"
                    str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " type does not match the database"
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Yellow
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Yellow
                Case 3
                    .DataGridView1(COL, ROW).Style.BackColor = Color.Green
                    .lbl_exist.Text = "The value already exists in the database"
                    str_err = "The ID " & .DataGridView1.Columns(COL).Name & "in a Row " & ROW + 2 & " already exists in the database"
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Green
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Green
                Case 4
                    .DataGridView1(COL, ROW).Style.BackColor = Color.Salmon
                    .lbl_invalid.Text = "Invalid date"
                    str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " invalid date"
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Salmon
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Salmon
                Case 5
                    .DataGridView1(COL, ROW).Style.BackColor = Color.Violet
                    .lbl_excess.Text = "Out of range of the string"
                    str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " string too big, The maximum size of the string is "
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Violet
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Violet
                Case 6
                    str_err = "The ID of column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " doesn't exist in related table"
                    .lbl_related.Text = "Does not exist in the related table"
                    .DataGridView1(COL, ROW).Style.BackColor = Color.SandyBrown
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.SandyBrown
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.SandyBrown
                Case 7
                    str_err = "Error in Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " this field must be blank"
                    .lbl_related.Text = "This field must be blank"
                    .DataGridView1(COL, ROW).Style.BackColor = Color.AliceBlue
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
                Case 9
                    str_err = "The value alredy exist in the colums CollectionNumber1 and CollectionNumber2 and Row " & ROW + 2
                    .lbl_related.Text = "This field must be blank"
                    .DataGridView1(COL, ROW).Style.BackColor = Color.AliceBlue
                    .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
                    .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
            End Select
            num = FreeFile()
            FileOpen(num, path_err, OpenMode.Append)
            WriteLine(num, str_err)
            FileClose(num)
            .pone_visible_label(TIPO)
        End With
    End Sub

End Module
