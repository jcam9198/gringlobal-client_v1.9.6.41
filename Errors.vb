﻿Public Class Errors
    Private id_error As Integer
    Private english As String
    Private spanish As String
    Private color As String




    Public Sub New(ByVal _id_color As Integer)
        id_error = _id_color
    End Sub
    Public Function Load_error() As Boolean
        Load_error = False
        Dim DTT As New DataTable
        DTT = carga_data_reader("Select english, spanish, color from errors where id_error = " & id_error, "configuration")
        If DTT.Rows.Count = 0 Then
            MsgBox("Error not Found")
            Exit Function
        End If
        With DTT.Rows(0)
            english = .Item("english").ToString
            spanish = .Item("spanish").ToString
            color = .Item("color").ToString


        End With
        Load_error = True
    End Function
    ReadOnly Property eng() As String
        Get
            Return english
        End Get
    End Property
    ReadOnly Property spn() As String
        Get
            Return spanish
        End Get
    End Property
    ReadOnly Property clr() As String
        Get
            Return color
        End Get
    End Property
End Class
