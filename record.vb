﻿Public Class record
    Private column_name As String
    Private value As String
    Private err As Integer
    Private row As Integer
    Private wich_one As String
    Private accid As String
    Private ct As crop_trait
    Private if_ct As Boolean = False
    Private if_table As Boolean = False
    Private if_table_alt As Boolean = False
    Private tb As table_field
    Private tb_alt As table_field

    Public Sub New(ByVal _column_name As String, ByVal _value As String, ByVal _row As String, ByVal _wich_one As String)
        wich_one = _wich_one
        column_name = _column_name
        value = _value
        row = _row
    End Sub
    ReadOnly Property get_mistake() As Integer
        Get
            Return err
        End Get
    End Property

    Public Sub record_checks()
        err = 0
        Dim dtt As New DataTable
        dtt = carga_data_reader("select * from CONFIGURATION_" & wich_one & " where encabezado = '" & column_name & "'", "configuration")
        If dtt.Rows.Count = 0 Then
            err = 10
            Exit Sub
        End If
        If value = "" Then
            If dtt.Rows(0).Item("nullable").ToString = "N" Then
                err = 1
                Exit Sub
            Else
                err = 0
                Exit Sub
            End If
        End If
        With dtt.Rows(0)
            If .Item("crop_trait_id").ToString <> "" Then
                ct = New crop_trait(.Item("crop_trait_id").ToString, value, wich_one, row, .Item("compound_rank_crop_trait").ToString, .Item("code_value_crop_trait").ToString)
                err = ct.check_crop_trait
                if_ct = True
            End If
            If .Item("table").ToString <> "" Then
                tb = New table_field(value, .Item("table").ToString, .Item("field").ToString, .Item("code_value_field").ToString, .Item("compound_rank").ToString, row, False)
                err = tb.check_table_field
                if_table = True
            End If
            If .Item("table_alt").ToString <> "" Then
                tb = New table_field(value, .Item("table_alt").ToString, .Item("field_alt").ToString, .Item("code_value_field_alt").ToString, .Item("compound_rank_alt").ToString, row, True)
                err = tb.check_table_field
                if_table = True
            End If
            If .Item("accession_inv_name").ToString <> "" Then

            End If
        End With
        'If dtt.Rows(0).Item("crop_trait_id").ToString <> "" Then

        '    record_checks = verifica_crop_trait(valor, dtt.Rows(0).Item("code_value_crop_trait").ToString, dtt.Rows(0).Item("crop_trait_id").ToString, dtt.Rows(0).Item("compound_rank_crop_trait").ToString, row)
        'End If
        'If dtt.Rows(0).Item("accession_inv_name").ToString <> "" Then
        '    record_checks = verifica_accession_inv_name(valor, dtt.Rows(0).Item("accession_inv_name").ToString, dtt.Rows(0).Item("code_value_accession_inv_name").ToString, dtt.Rows(0).Item("compound_rank_accession_inv_name").ToString, dtt.Rows(0).Item("compound_link_accession_inv_name").ToString, row)
        'End If
        'If dtt.Rows(0).Item("table").ToString <> "" Then
        '    record_checks = verifica_table(valor, dtt.Rows(0).Item("table").ToString, dtt.Rows(0).Item("field").ToString, dtt.Rows(0).Item("code_value_field").ToString, dtt.Rows(0).Item("compound_rank").ToString, dtt.Rows(0).Item("compound_link").ToString, row)
        'End If

    End Sub
    Public Function verifica_registro(ByVal valor As String, ByVal columna As String, ByVal de_cual As String, ByVal row As Integer) As Integer
        verifica_registro = 0
        Dim dtt As New DataTable
        dtt = carga_data_reader("select * from CONFIGURATION_" & de_cual & " where encabezado = '" & columna & "'", "configuration")
        If dtt.Rows.Count = 0 Then
            verifica_registro = 10
            Exit Function
        End If



    End Function


End Class
