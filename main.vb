﻿Option Explicit On
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data.OleDb
Module main
    Public name_servidor As String
    Public cnx_excel As OleDbConnection
    Public adt_excel As OleDbDataAdapter
    Public dts_excel As DataSet
    Public que_tiene_cargado As String
    Public path_excel As String
    Public str_cnx As String
    Public tipo_error As Integer
    Public user As String
    Public pwd As String
    Public sigue As Byte
    Public cnx As SqlConnection
    Public adt As SqlDataAdapter
    Public dts As DataSet
    Public dtv As DataView
    Public cmd As SqlCommandBuilder
    Public path_err As String
    Public path_aplicacion As String
    Public tam_max As Integer
    Public Sub main()
        sigue = 1
        path_aplicacion = CurDir()
        path_aplicacion = path_aplicacion & "\"
        'CONECTION.ShowDialog()

        'If sigue = 1 Then UPDATEMGB.ShowDialog()
        'End
        UPDATEMGB.ShowDialog()
    End Sub
    Public Sub limpia_todo()
        'If cnx.State = ConnectionState.Open Then cnx.Close()
        If que_tiene_cargado = "REGEN" Or que_tiene_cargado = "PASSPORT" Or que_tiene_cargado = "COOP_COOPMGB" Then
            If dts_enc.Tables(0).Rows.Count > 0 Then dts_enc.Tables(0).Clear()
            dts_enc.Dispose()
            adt_enc.Dispose()
            If cnx_enc.State = ConnectionState.Open Then cnx_enc.Close()
        Else
            dtv.Dispose()
            dts.Tables(0).Clear()
            dts.Dispose()
            adt.Dispose()
        End If
        If cnx_excel.State = ConnectionState.Open Then cnx_excel.Close()
        UPDATEMGB.DataGridView1.DataSource = ""
        UPDATEMGB.dtg_list.Rows.Clear()
        LIMPIA_CNX_EXCEL()
        dts_excel.Tables(0).Clear()
        dts_excel.Dispose()
        adt_excel.Dispose()
    End Sub
    Public Function SAVE_TO_DATABASE() As Boolean
        SAVE_TO_DATABASE = False
        cmd = New SqlCommandBuilder(adt)
        NUM_OP = (dts_excel.Tables(0).Rows.Count - 1)
        UPDATEMGB.PGRBAR.Visible = True
        UPDATEMGB.PGRBAR.Minimum = 1
        UPDATEMGB.PGRBAR.Maximum = NUM_OP
        UPDATEMGB.PGRBAR.Value = 1
        UPDATEMGB.PGRBAR.Step = 1
        Dim i As Integer
        For i = 0 To dts_excel.Tables(0).Rows.Count - 1
            dts.Tables(0).Rows.Add(dts_excel.Tables(0).Rows(i).ItemArray)
            UPDATEMGB.PGRBAR.PerformStep()
        Next
        adt.InsertCommand = cmd.GetInsertCommand
        Try
            If dts.HasChanges Then
                adt.Update(dts, que_tiene_cargado)
            End If
            SAVE_TO_DATABASE = True
            MsgBox("Ready")
        Catch ex As Exception

            SAVE_TO_DATABASE = False
            MsgBox("ERROR: " & vbCrLf & ex.Message)
        End Try
    End Function

    Public Function conecta_a_excel() As Boolean
        Dim str As String
        Dim path As String
        Dim path_excel_xls As String
        Dim EXCEL2007 As Boolean
        EXCEL2007 = False
        conecta_a_excel = False
        Try
            Select Case que_tiene_cargado
                Case "COOP_COOPMGB"
                    path_excel = path_aplicacion & "COOP_COOPMGB.xlsx"
                Case "COOPMGB"
                    path_excel = path_aplicacion & "Coopmgb.xlsx"
                Case "INSTITUTION"
                    path_excel = path_aplicacion & "Institution.xlsx"
                Case "SEEDENT"
                    path_excel = path_aplicacion & "Seedent.xlsx"
                Case "SEEDSHIP"
                    path_excel = path_aplicacion & "Seedship.xlsx"
                Case "REGEN"
                    path_excel = path_aplicacion & "regen.xlsx"
                Case "PASSPORT"
                    path_excel = path_aplicacion & "Passport.txt"
            End Select
            If IO.File.Exists(path_excel) Then EXCEL2007 = True
            If Not EXCEL2007 Then
                Select Case que_tiene_cargado
                    Case "COOP"
                        path_excel_xls = path_aplicacion & "Coop_COOPMGB.xls"
                    Case "COOPMGB"
                        path_excel_xls = path_aplicacion & "Coopmgb.xls"
                    Case "INSTITUTION"
                        path_excel_xls = path_aplicacion & "Institution.xls"
                    Case "SEEDENT"
                        path_excel_xls = path_aplicacion & "Seedent.xls"
                    Case "SEEDSHIP"
                        path_excel_xls = path_aplicacion & "Seedship.xls"
                    Case "REGEN"
                        path_excel_xls = path_aplicacion & "REGEN.xls"
                    Case "PASSPORT"
                        path_excel_xls = path_aplicacion & "Passport.txt"

                End Select
                If Not IO.File.Exists(path_excel_xls) Then
                    MsgBox("File " + path_excel_xls + " or " & path_excel & " does not exist")
                    Exit Function
                End If
            End If
            If EXCEL2007 Then
                path = path_excel
                str = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & _
                        path_excel & ";Extended properties=""Excel 12.0;hdr=yes;imex=true;"""
            Else
                path = path_excel_xls
                str = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                        path_excel_xls & ";Extended properties=""Excel 8.0;hdr=yes;imex=true;"""
            End If

            'cnx_excel = New OleDbConnection(str)
            'cnx_excel.Open()
            'If Not cnx_excel.State = ConnectionState.Open Then
            '    MsgBox("Can not connect with excel sheet")
            '    Exit Function
            'End If
            'str = "Select * from [" + que_tiene_cargado + "$]"
            'adt_excel = New OleDbDataAdapter(str, cnx_excel)
            'dts_excel = New DataSet
            'adt_excel.Fill(dts_excel, que_tiene_cargado)

            Dim data_set As New dts(path_excel)
            data_set.Charge_dataset()
            If data_set.state = False Then
                MsgBox("Can not connect with excel sheet")
                Exit Function
            End If
            dts_excel = New DataSet
            dts_excel.Tables.Add(data_set.dtt)

            If que_tiene_cargado = "REGEN" Or que_tiene_cargado = "PASSPORT" Or que_tiene_cargado = "COOP_COOPMGB" Then
                conecta_a_excel = True
                Exit Function
            End If
            str = "Select * from " & que_tiene_cargado
            adt = New SqlDataAdapter(str, cnx)
            dts = New DataSet
            adt.Fill(dts, que_tiene_cargado)
            dtv = New DataView(dts.Tables(0))
            If dts.Tables(0).Columns.Count <> dts_excel.Tables(0).Columns.Count Then
                MsgBox("The number of columns does not match the database")
                Exit Function
            End If
            Dim i As Integer
            Dim namebds As String
            Dim nameexe As String
            For i = 0 To dts.Tables(0).Columns.Count - 1
                namebds = UCase(Trim(dts.Tables(0).Columns.Item(i).ColumnName))
                nameexe = UCase(Trim(dts_excel.Tables(0).Columns.Item(i).ColumnName))
                If Not namebds = nameexe Then
                    MsgBox("The name of columns does not match the database")
                    Exit Function
                End If
            Next i
            UPDATEMGB.DataGridView1.DataSource = dts_excel.Tables(0)
            conecta_a_excel = True
        Catch ex As Exception
            conecta_a_excel = False
            MsgBox("ERROR: " & vbCrLf & ex.Message)
            LIMPIA_CNX_EXCEL()
        End Try
    End Function
    Public Sub LIMPIA_CNX_EXCEL()
        cnx_excel.Dispose()
    End Sub
    Public Function VALIDA_DATOS(ByVal CUAL As Byte) As Boolean
        Dim num_ar As Integer
        path_err = path_aplicacion & que_tiene_cargado & ".txt"
        num_ar = FreeFile()
        FileOpen(num_ar, path_err, OpenMode.Output)
        FileClose(num_ar)
        Select Case CUAL
            
            Case 3
                VALIDA_DATOS = VALIDA_INSTITUTION()
            Case 4
                VALIDA_DATOS = VALIDA_SEEDENT()
            Case 5
                VALIDA_DATOS = VALIDA_SEEDSHIP()
        End Select
    End Function
    Public Function VALIDA_SEEDSHIP() As Boolean
        Dim col As Integer
        Dim row As Integer
        Dim ind As Integer
        VALIDA_SEEDSHIP = True
        ind = 0
        With UPDATEMGB
            For row = 0 To .DataGridView1.RowCount - 2
                For col = 0 To .DataGridView1.ColumnCount - 1
                    tipo_error = 0
                    Select Case col
                        Case 0
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 2
                            ElseIf existe_seedship(CInt(.DataGridView1(col, row).Value.ToString)) Then
                                tipo_error = 3
                            End If
                        Case 1
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Len(.DataGridView1(col, row).Value.ToString) > 50 Then
                                tipo_error = 5
                                tam_max = 50
                            ElseIf valida_ccode_coop(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 6
                            End If
                        Case 2
                            If Len(.DataGridView1(col, row).Value.ToString) > 8 Then
                                tipo_error = 5
                                tam_max = 8
                            End If

                        Case 3
                            If Len(.DataGridView1(col, row).Value.ToString) > 6 Then
                                tipo_error = 5
                                tam_max = 6
                            End If

                        Case 4
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 2
                            End If
                        Case 5
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Len(.DataGridView1(col, row).Value.ToString) > 8 Then
                                tipo_error = 5
                                tam_max = 8
                            End If
                        Case 6
                            If Len(.DataGridView1(col, row).Value.ToString) > 150 Then
                                tipo_error = 5
                                tam_max = 150
                            End If
                    End Select
                    If tipo_error > 0 Then
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                        VALIDA_SEEDSHIP = False
                    End If
                Next col
            Next row
        End With
    End Function
    Public Function VALIDA_SEEDENT() As Boolean
        Dim col As Integer
        Dim row As Integer
        Dim ind As Integer = 0
        VALIDA_SEEDENT = True
        With UPDATEMGB
            For row = 0 To .DataGridView1.RowCount - 2
                If (.DataGridView1(0, row).Value.ToString) <> "" And _
                   (.DataGridView1(1, row).Value.ToString) <> "" And _
                   (.DataGridView1(2, row).Value.ToString) <> "" And _
                    IsNumeric((.DataGridView1(0, row).Value.ToString)) And _
                    IsNumeric((.DataGridView1(2, row).Value.ToString)) Then
                    If existe_seedent(CInt(.DataGridView1(0, row).Value.ToString), _
                                   (.DataGridView1(1, row).Value.ToString), _
                                   CInt(.DataGridView1(2, row).Value.ToString)) Then
                        .DataGridView1(0, row).Style.BackColor = Color.Green
                        .DataGridView1(1, row).Style.BackColor = Color.Green
                        MARCA_ERROR(3, 2, row, tam_max)
                        GoTo brica_for
                    End If
                End If
                For col = 0 To .DataGridView1.ColumnCount - 1
                    tipo_error = 0
                    Select Case col
                        Case 0
                            If .DataGridView1(col, row).Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 2
                            ElseIf valida_seedshipmentid_ship(CInt(.DataGridView1(col, row).Value.ToString)) Then
                                tipo_error = 6
                            End If
                        Case 1
                            If .DataGridView1(col, row).Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf Len(.DataGridView1(col, row).Value.ToString) > 50 Then
                                tipo_error = 5
                                tam_max = 50
                            ElseIf valida_accid(.DataGridView1(col, row).Value.ToString) Then

                            End If
                        Case 2
                            If .DataGridView1(col, row).Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 2
                            End If
                        Case 3, 4
                            If .DataGridView1(col, row).Value.ToString <> "" And Not IsNumeric(.DataGridView1(col, row).Value.ToString = "") Then
                                tipo_error = 2
                            End If
                    End Select
                    If tipo_error > 0 Then
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                        VALIDA_SEEDENT = False
                    End If
                Next col
brica_for:
            Next row
        End With
    End Function
    Public Function VALIDA_INSTITUTION() As Boolean
        Dim col As Integer
        Dim row As Integer
        VALIDA_INSTITUTION = True
        With UPDATEMGB
            For row = 0 To .DataGridView1.RowCount - 2
                For col = 0 To .DataGridView1.ColumnCount - 1
                    tipo_error = 0
                    Select Case col
                        Case 0
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 2
                            ElseIf existe_institution(CInt(.DataGridView1(col, row).Value.ToString)) Then
                                tipo_error = 3
                            End If
                        Case 1
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Len(.DataGridView1(col, row).Value.ToString) > 50 Then
                                tipo_error = 5
                                tam_max = 50
                            End If
                        Case 2, 3, 4, 5, 6, 7
                            If Len(.DataGridView1(col, row).Value.ToString) > 50 Then
                                tipo_error = 5
                                tam_max = 50
                            End If

                        Case 8
                            If (.DataGridView1(col, row).Value.ToString) = "" Then
                                tipo_error = 1
                            ElseIf Len(.DataGridView1(col, row).Value.ToString) > 10 Then
                                tipo_error = 5
                                tam_max = 10
                            ElseIf valida_iso(.DataGridView1(col, row).Value.ToString) Then
                                tipo_error = 6
                            End If
                        Case 9, 10, 11
                            If Len(.DataGridView1(col, row).Value.ToString) > 20 Then
                                tipo_error = 5
                                tam_max = 20
                            End If

                        Case 12
                            If Len(.DataGridView1(col, row).Value.ToString) > 80 Then
                                tipo_error = 5
                                tam_max = 80
                            End If

                    End Select

                    If tipo_error > 0 Then
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                        VALIDA_INSTITUTION = False
                    End If
                Next col

            Next row
        End With
    End Function
    Public Sub mark_error(ByVal type As Integer, ByVal col As Integer, ByVal row As Integer)
        If SEARCH_LABEL("label_" & CStr(type)) Then Exit Sub

        Dim err As New Errors(type)
        If err.Load_error Then
            With UPDATEMGB.p_error
                Dim l As Label
                l.Name = "label_" & CStr(type)
                l.Text = err.eng
                l.BackColor = Color.FromName(err.clr)
            End With
        End If



    End Sub
    Public Function SEARCH_LABEL(ByVal NAME As String) As Boolean
        SEARCH_LABEL = False
        For Each L As Label In UPDATEMGB.p_error.Controls
            If L.Name = NAME Then
                SEARCH_LABEL = True
            End If
        Next

    End Function
    'Public Sub MARCA_ERROR(ByVal TIPO As Byte, ByVal COL As Integer, ByVal ROW As Integer, ByVal tammax As Integer)
    '    Dim num As Integer
    '    Dim str_err As String
    '    With UPDATEMGB
    '        .dtg_list.Rows.Add(COL + 1, ROW + 1)

    '        Select Case TIPO
    '            Case 1
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.Red
    '                .lbl_null.Text = "This value can not be null"
    '                str_err = "Error in Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " the field can't be Null"
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Red
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Red
    '            Case 2
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.Yellow
    '                .lbl_incompatible.Text = "Incompatible value"
    '                str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " type does not match the database"
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Yellow
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Yellow
    '            Case 3
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.Green
    '                .lbl_exist.Text = "The value already exists in the database"
    '                str_err = "The ID " & .DataGridView1.Columns(COL).Name & "in a Row " & ROW + 2 & " already exists in the database"
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Green
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Green
    '            Case 4
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.Salmon
    '                .lbl_invalid.Text = "Invalid date"
    '                str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " invalid date"
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Salmon
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Salmon
    '            Case 5
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.Violet
    '                .lbl_excess.Text = "Out of range of the string"
    '                str_err = "Error en Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " string too big, The maximum size of the string is " & tammax
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.Violet
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.Violet
    '            Case 6
    '                str_err = "The ID of column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " doesn't exist in related table"
    '                .lbl_related.Text = "Does not exist in the related table"
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.SandyBrown
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.SandyBrown
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.SandyBrown
    '            Case 7
    '                str_err = "Error in Column " & .DataGridView1.Columns(COL).Name & " and Row " & ROW + 2 & " this field must be blank"
    '                .lbl_related.Text = "This field must be blank"
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.AliceBlue
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
    '            Case 9
    '                str_err = "The value alredy exist in the colums CollectionNumber1 and CollectionNumber2 and Row " & ROW + 2
    '                .lbl_related.Text = "This field must be blank"
    '                .DataGridView1(COL, ROW).Style.BackColor = Color.AliceBlue
    '                .dtg_list(0, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
    '                .dtg_list(1, .dtg_list.RowCount - 2).Style.BackColor = Color.AliceBlue
    '        End Select
    '        num = FreeFile()
    '        FileOpen(num, path_err, OpenMode.Append)
    '        WriteLine(num, str_err)
    '        FileClose(num)
    '        .pone_visible_label(TIPO)
    '    End With
    'End Sub
    'Public Function VALIDA_COOPMGB() As Boolean
    '    Dim col As Integer
    '    Dim row As Integer
    '    Dim ind As Integer = 0
    '    VALIDA_COOPMGB = True
    '    With UPDATEMGB
    '        For row = 0 To .DataGridView1.RowCount - 2
    '            For col = 0 To .DataGridView1.ColumnCount - 1
    '                tipo_error = 0
    '                Select Case col
    '                    Case 0
    '                        If (.DataGridView1(col, row).Value.ToString) = "" Then
    '                            tipo_error = 1
    '                        ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
    '                            tipo_error = 2
    '                        ElseIf existe_coopmgb(CInt(.DataGridView1(col, row).Value.ToString)) Then
    '                            tipo_error = 3
    '                        End If
    '                    Case 1
    '                        If (.DataGridView1(col, row).Value.ToString) = "" Then
    '                            tipo_error = 1
    '                        ElseIf Len(.DataGridView1(col, row).Value.ToString) > 50 Then
    '                            tipo_error = 5
    '                            tam_max = 50
    '                        ElseIf valida_ccode_coop(.DataGridView1(col, row).Value.ToString) Then
    '                            tipo_error = 6
    '                        End If
    '                    Case 2
    '                        If .DataGridView1(col, row).Value.ToString = "" Then
    '                            tipo_error = 1
    '                        ElseIf Not IsNumeric(.DataGridView1(col, row).Value.ToString) Then
    '                            tipo_error = 2
    '                        ElseIf valida_coopmgb_intitutionid(CInt(.DataGridView1(col, row).Value.ToString)) Then
    '                            tipo_error = 6
    '                        End If
    '                    Case 3, 4, 5, 6, 7, 8
    '                        If Len(.DataGridView1(col, row).Value.ToString) > 50 Then
    '                            tipo_error = 5
    '                            tam_max = 50
    '                        End If
    '                    Case 9, 10, 11
    '                        If Len(.DataGridView1(col, row).Value.ToString) > 20 Then
    '                            tipo_error = 5
    '                            tam_max = 20
    '                        End If

    '                    Case 12
    '                        If Len(.DataGridView1(col, row).Value.ToString) > 80 Then
    '                            tipo_error = 5
    '                            tam_max = 50
    '                        End If

    '                End Select
    '                If tipo_error > 0 Then
    '                    MARCA_ERROR(tipo_error, col, row, tam_max)
    '                    VALIDA_COOPMGB = False
    '                End If
    '            Next col
    '        Next row
    '    End With
    'End Function


    Public Function valida_accid(ByVal accid As String) As Boolean
        valida_accid = False
        Dim cmd As SqlCommand
        Dim num As String
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select * from geo where iso = '" & accid & "'"
        num = cmd.ExecuteScalar
        If Len(Trim(num)) = 0 Then valida_accid = True

    End Function
    Public Function valida_usercode(ByVal usercode As Integer) As Boolean
        valida_usercode = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select usercode from coduser where usercode = " & usercode
        num = cmd.ExecuteScalar
        If num = 0 Then valida_usercode = True
    End Function
    Public Function valida_iso(ByVal iso As String) As Boolean
        valida_iso = False
        Dim cmd As SqlCommand
        Dim num As String
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select * from geo where iso = '" & iso & "'"
        num = cmd.ExecuteScalar
        If Len(Trim(num)) = 0 Then valida_iso = True
    End Function
    Public Function valida_ccode_coop(ByVal id As String) As Boolean
        valida_ccode_coop = False
        Dim cmd As SqlCommand
        Dim num As String
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select * from coop where ccode = '" & id & "'"
        num = cmd.ExecuteScalar
        If Len(Trim(num)) = 0 Then valida_ccode_coop = True
    End Function
    Public Function valida_seedshipmentid_ship(ByVal id As Integer) As Boolean
        valida_seedshipmentid_ship = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select * from seedship where seedshipmentid = " & id
        num = cmd.ExecuteScalar
        If num = 0 Then valida_seedshipmentid_ship = True
    End Function
    Public Function valida_coopmgb_intitutionid(ByVal id As Integer) As Boolean
        valida_coopmgb_intitutionid = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select * from institution where institutionid = " & id
        num = cmd.ExecuteScalar
        If num = 0 Then valida_coopmgb_intitutionid = True
    End Function
    Public Function existe_seedship(ByVal seedshipmentID As Integer) As Boolean
        existe_seedship = False
        dtv.RowFilter = "SeedshipmentID = " & seedshipmentID
        If dtv.Count > 0 Then existe_seedship = True
    End Function
    Public Function existe_seedent(ByVal seedshipmentid As Integer, ByVal accid As String, ByVal entrynumber As Integer) As Boolean
        existe_seedent = False
        dtv.RowFilter = "SeedshipmentID = " & seedshipmentid & _
                       "and AccID = '" & accid & "' and EntryNumber = " & entrynumber
        If dtv.Count > 0 Then existe_seedent = True
    End Function
    Public Function existe_institution(ByVal institutionid As Integer) As Boolean
        existe_institution = False
        dtv.RowFilter = "InstitutionID = " & institutionid
        If dtv.Count > 0 Then existe_institution = True
    End Function
    'Public Function existe_coopmgb(ByVal cooperatorid As Integer) As Boolean
    '    existe_coopmgb = False
    '    dtv.RowFilter = "CooperatorID = " & cooperatorid
    '    If dtv.Count > 0 Then existe_coopmgb = True
    'End Function
    'Public Function existe_ccode(ByVal ccode As String) As Boolean
    '    existe_ccode = False
    '    dtv.RowFilter = "ccode = '" & ccode & "'"
    '    If dtv.Count > 0 Then existe_ccode = True
    'End Function

    Public Function conection_to_dbs() As Boolean
        Try
            conection_to_dbs = False
            str_cnx = "Data Source=" + name_servidor + ";Initial Catalog=MGB;User ID=" + user + ";Password=" + pwd + ";"
            cnx = New SqlConnection(str_cnx)
            If cnx.State = ConnectionState.Closed Then cnx.Open()
            If cnx.State = ConnectionState.Open Then conection_to_dbs = True
        Catch ex As Exception
            MsgBox("ERROR: " & vbCrLf & ex.Message)
        End Try
    End Function

    Public Function checa_fecha(ByVal fecha As String) As Boolean
        checa_fecha = False
        If Not IsNumeric(fecha) Then Exit Function
        If Len(Trim(fecha)) <> 8 Then Exit Function
        Dim dia As String
        Dim mes As String
        Dim ano As String
        dia = Mid(fecha, 7, 2)
        mes = Mid(fecha, 5, 2)
        ano = Mid(fecha, 1, 4)
        If IsDate(mes & "/" & dia & "/" & ano) Then checa_fecha = True
    End Function
End Module
