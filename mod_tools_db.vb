﻿Imports System.Data.SqlClient
Module mod_tools_db
    Public Function connection(ByVal which_connection As String) As SqlConnection
        Dim str As String
        If which_connection = "configuration" Then
            str = "Data Source=GRESOLORZANO2-N\SQL_SERVER_2008; Initial Catalog=GGWORK;User ID=sa;Password=Cachimbas1;"
        Else
            str = "Data Source=GRESOLORZANO2-N\SQL_SERVER_2008; Initial Catalog=gringlobal;User ID=sa;Password=Cachimbas1;"
        End If
        connection = New SqlConnection(str)
        If connection.State = ConnectionState.Closed Then connection.Open()
    End Function

    Public Function return_integer(ByVal str As String, ByVal which_connection As String) As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand
        cmd.Connection = connection(which_connection)
        cmd.CommandType = CommandType.Text
        cmd.CommandText = str
        return_integer = Convert.ToString(cmd.ExecuteScalar)
    End Function
    Public Function ejecuta_query(ByVal str As String, ByVal which_connection As String) As Boolean
        ejecuta_query = False
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand
            cmd.Connection = connection(which_connection)
            cmd.CommandType = CommandType.Text
            cmd.CommandText = str
            cmd.ExecuteNonQuery()
            ejecuta_query = True
        Catch ex As Exception
            MsgBox(ex.Message)
            ejecuta_query = False
        End Try
    End Function
    Public Function carga_data_reader(ByVal str As String, ByVal which_connection As String) As DataTable
        Dim cmd As SqlCommand
        Dim dtr As SqlDataReader
        Dim dtt As New DataTable
        cmd = New SqlCommand
        cmd.Connection = connection(which_connection)
        cmd.CommandType = CommandType.Text
        cmd.CommandText = str
        dtr = cmd.ExecuteReader
        dtt.Load(dtr)
        carga_data_reader = dtt
    End Function

End Module
