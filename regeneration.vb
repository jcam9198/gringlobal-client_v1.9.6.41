﻿Option Explicit On
Imports Microsoft.Office.Interop.Excel
Imports System.Data.OleDb
Imports System.Data.SqlClient

Module regeneration
    Public cnx_enc As SqlConnection
    Public adt_enc As SqlDataAdapter
    Public dts_enc As DataSet
    Public arr_encabezado() As String
    Public arr_tabla_gg() As String
    Public arr_nombrebds() As String
    Public arr_tipodato() As String
    Public arr_maxvalor() As Integer
    Public arr_null() As Integer
    Public arr_maxpro() As Integer
    Public arr_valmin() As String
    Public arr_valmax() As String
    Public cuantos_encabezado As Integer
    Public cad_cmd As String
    Public NUM_OP As Integer
    Public tran As SqlTransaction
    Public Function carga_encabezado_bds(ByVal DE_CUAL As String) As Boolean
        Dim STR As String
        Try
            STR = "select * from encabezado_" & DE_CUAL
            adt_enc = New SqlDataAdapter(STR, cnx)
            dts_enc = New DataSet
            adt_enc.Fill(dts_enc, DE_CUAL)

            cuantos_encabezado = dts_enc.Tables(0).Rows.Count
            ReDim arr_encabezado(cuantos_encabezado)
            ReDim arr_maxpro(cuantos_encabezado)
            ReDim arr_maxvalor(cuantos_encabezado)
            ReDim arr_nombrebds(cuantos_encabezado)
            ReDim arr_null(cuantos_encabezado)
            ReDim arr_tipodato(cuantos_encabezado)
            ReDim arr_valmax(cuantos_encabezado)
            ReDim arr_valmin(cuantos_encabezado)
            For i = 0 To dts_enc.Tables(0).Rows.Count - 1
                arr_encabezado(i) = dts_enc.Tables(0).Rows(i).Item("Encabezado_excel").ToString
                arr_nombrebds(i) = dts_enc.Tables(0).Rows(i).Item("Nombre_en_BDS").ToString
                arr_tipodato(i) = dts_enc.Tables(0).Rows(i).Item("Tipo_de_dato").ToString
                arr_maxvalor(i) = dts_enc.Tables(0).Rows(i).Item("Max").ToString
                arr_null(i) = dts_enc.Tables(0).Rows(i).Item("Nulls").ToString
                arr_maxpro(i) = CInt(dts_enc.Tables(0).Rows(i).Item("max_pro").ToString)
                arr_valmin(i) = dts_enc.Tables(0).Rows(i).Item("val_min").ToString
                arr_valmax(i) = dts_enc.Tables(0).Rows(i).Item("val_max").ToString
            Next i
            carga_encabezado_bds = True

        Catch ex As Exception
            MsgBox(ex.Message)
            carga_encabezado_bds = False
        End Try
    End Function

    Public Function compara_regeneracion() As Boolean
        Dim i As Integer
        compara_regeneracion = False
        'MsgBox(dts_excel.Tables(0).Columns.Count)
        If cuantos_encabezado <> dts_excel.Tables(0).Columns.Count Then
            MsgBox("The number of columns does not match the database")
            Exit Function
        End If
        Dim nom_enc As String
        Dim nom_REG As String
        For i = 0 To cuantos_encabezado - 1
            nom_enc = UCase(Trim(arr_encabezado(i)))
            nom_REG = UCase(Trim(dts_excel.Tables(0).Columns(i).ColumnName.ToString))
            If nom_enc <> nom_REG Then
                MsgBox("The name of columns " & nom_REG & " does not match int the database")
                Exit Function
            End If
        Next
        compara_regeneracion = True
    End Function
    Public Function COMPARA_DATOS_COOP() As Boolean
        Dim row As Integer
        Dim col As Integer
        COMPARA_DATOS_COOP = True

        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
                tipo_error = 0
                With UPDATEMGB.DataGridView1(col, row)
                    Select Case col
                        Case 0
                            If .Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf Len(.Value.ToString) > 50 Then
                                tipo_error = 5
                                tam_max = 50
                            ElseIf existe_ccode(.Value.ToString) Then
                                tipo_error = 3
                            End If
                        Case 6
                            If .Value.ToString = "" Then
                                tipo_error = 1
                            Else
                                tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                If tipo_error = 0 Then
                                    If Not existe_institution(CInt(.Value.ToString)) Then
                                        tipo_error = 6
                                    End If
                                End If
                            End If
                        Case 15
                            If .Value.ToString = "" Then
                                tipo_error = 1
                            Else
                                tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                If tipo_error = 0 Then
                                    If existe_coopmgb(CInt(.Value.ToString)) Then
                                        tipo_error = 3
                                    End If
                                End If
                            End If
                        Case Else
                            If CInt(arr_null(col)) = 1 And .Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf .Value.ToString <> "" Then
                                tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                If tipo_error = 0 Then
                                    If arr_valmin(col) <> "" And arr_valmax(col) <> "" Then
                                        tipo_error = verifica_max_min_interno(.Value.ToString, arr_tipodato(col), arr_valmin(col), arr_valmax(col))
                                    End If
                                End If
                            End If

                           
                    End Select
                    If tipo_error > 0 Then
                        COMPARA_DATOS_COOP = False
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                    End If
                End With
            Next
        Next


    End Function
    Public Function existe_en_token(ByVal valor As String, ByVal valores_permitidos As String) As Boolean
        existe_en_token = False
        Dim cad() As String = valores_permitidos.Split(",")
        Dim i As Integer
        For i = 0 To cad.Count - 1
            If valor = cad(i) Then
                existe_en_token = True
                Exit Function
            End If
        Next
    End Function
    Public Function compara_datos_passport() As Boolean
        Dim row As Integer
        Dim col As Integer
        Dim ACCID As Integer
        Dim ID_UNICO As String
        Dim ERROR_COLLECTION As Boolean
        ACCID = devuelve_posible_acc()
        compara_datos_passport = True
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            ERROR_COLLECTION = False
            ID_UNICO = LTrim(RTrim(UPDATEMGB.DataGridView1(38, row).Value.ToString)) & LTrim(RTrim(UPDATEMGB.DataGridView1(39, row).Value.ToString))
            If ID_UNICO = "" Then
                MARCA_ERROR(1, 38, row, tam_max)
                MARCA_ERROR(1, 39, row, tam_max)
                ERROR_COLLECTION = True
                compara_datos_passport = False
            ElseIf existe_collectionnumber_in_db(LTrim(RTrim((UPDATEMGB.DataGridView1(38, row).Value.ToString))), _
                                                 LTrim(RTrim(UPDATEMGB.DataGridView1(39, row).Value.ToString))) Then
                MARCA_ERROR(3, 38, row, tam_max)
                MARCA_ERROR(3, 39, row, tam_max)
                ERROR_COLLECTION = True
                compara_datos_passport = False
            ElseIf existe_id_collectionnumber(ID_UNICO, row) Then
                MARCA_ERROR(9, 0, row, tam_max)
                ERROR_COLLECTION = True
                compara_datos_passport = False
            End If
            For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
                With UPDATEMGB.DataGridView1(col, row)
                    tipo_error = 0
                    If CInt(arr_null(col)) Then
                        If .Value.ToString = "" Then
                            tipo_error = 1
                        End If
                    End If
                    If tipo_error = 0 Then
                        Select Case col
                            Case 0
                                If .Value.ToString <> "" Then
                                    tipo_error = 7
                                Else
                                    .Value = "CIMMYTMA-" + llena_ceros(CStr(ACCID))
                                    .Style.BackColor = Color.DeepSkyBlue
                                    UPDATEMGB.pone_visible_label(8)
                                End If
                            Case 1
                                If .Value.ToString <> "" Then
                                    tipo_error = 7
                                End If
                            Case 5
                                If .Value.ToString <> "" Then
                                    tipo_error = verifica_primrayrace(.Value.ToString)
                                End If
                            Case 35, 41, 65
                                If .Value.ToString <> "" Then
                                    tipo_error = verifica_fecha_correcta(.Value)
                                End If
                            Case 38, 39
                                If Not ERROR_COLLECTION Then
                                    tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                    If tipo_error = 0 Then
                                        If arr_valmin(col) <> "" And arr_valmax(col) <> "" Then
                                            tipo_error = verifica_max_min_interno(.Value.ToString, arr_tipodato(col), arr_valmin(col), arr_valmax(col))
                                        End If
                                    End If
                                End If
                            Case Else
                                If CInt(arr_null(col)) = 1 And .Value.ToString = "" Then
                                    tipo_error = 1
                                ElseIf .Value.ToString <> "" Then
                                    tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                    If tipo_error = 0 Then
                                        If arr_valmin(col) <> "" And arr_valmax(col) <> "" Then
                                            tipo_error = verifica_max_min_interno(.Value.ToString, arr_tipodato(col), arr_valmin(col), arr_valmax(col))
                                        End If
                                    End If
                                End If
                        End Select
                    End If
                    If tipo_error > 0 Then
                        compara_datos_passport = False
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                    End If
                End With
            Next col
            ACCID = ACCID + 1
        Next row
    End Function
    Public Function compara_datos_regeneracion() As Boolean
        Dim row As Integer
        Dim col As Integer
        compara_datos_regeneracion = True
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
                With UPDATEMGB.DataGridView1(col, row)
                    tipo_error = 0

                    Select Case col
                        'Case 12, 15
                        '    tipo_error = verifica_mes_dia(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString)
                        '    If tipo_error > 0 Then
                        '        compara_datos_regeneracion = False
                        '        MARCA_ERROR(tipo_error, col, row, tam_max)
                        '    End If
                        '    col = col + 1

                        Case 82, 86, 94, 98, 102
                            If .Value.ToString = "" Then
                                If Not busca_campos_vacios(col, row) Then
                                    tipo_error = 4

                                End If

                            ElseIf Not IsNumeric(.Value.ToString) Then
                                tipo_error = 2

                            Else
                                If busca_campos_vacios(col, row) Then
                                    tipo_error = 4

                                End If
                            End If
                        Case 69
                            tipo_error = verifica_fecha_previus(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString, UPDATEMGB.DataGridView1(col + 2, row).Value.ToString)
                            If tipo_error > 0 Then
                                compara_datos_regeneracion = False
                                MARCA_ERROR(tipo_error, col, row, tam_max)
                                MARCA_ERROR(tipo_error, col + 1, row, tam_max)
                            End If
                            col = col + 2
                        Case 12, 16, 75, 79, 83, 87, 91, 95, 99, 103
                            tipo_error = verifica_fecha(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString, UPDATEMGB.DataGridView1(col + 2, row).Value.ToString)
                            If tipo_error > 0 Then
                                compara_datos_regeneracion = False
                                MARCA_ERROR(tipo_error, col, row, tam_max)
                                MARCA_ERROR(tipo_error, col + 1, row, tam_max)
                            End If
                            col = col + 2

                        Case Else
                            If CInt(arr_null(col)) = 1 And .Value.ToString = "" Then
                                tipo_error = 1
                            ElseIf .Value.ToString <> "" Then
                                tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
                                If tipo_error = 0 Then
                                    If arr_valmin(col) <> "" And arr_valmax(col) <> "" Then
                                        tipo_error = verifica_max_min_interno(.Value.ToString, arr_tipodato(col), arr_valmin(col), arr_valmax(col))
                                    End If
                                End If
                            End If

                    End Select
                    If tipo_error > 0 Then
                        compara_datos_regeneracion = False
                        MARCA_ERROR(tipo_error, col, row, tam_max)
                    End If
                End With
            Next
        Next


    End Function
    Public Function verifica_fecha_previus(ByVal day As String, ByVal month As String, ByVal year As String) As Integer
        verifica_fecha_previus = 4
        If day = "" And month = "" And year = "" Then
            verifica_fecha_previus = 0
            Exit Function
        End If
        If day = "" Then day = "01"
        If Not IsNumeric(day) Then Exit Function
        If CInt(day) < 1 And CInt(day) > 31 Then Exit Function
        If Len(day) = 1 Then day = "0" + day
        If month = "" Then month = "01"
        If Not IsNumeric(month) Then Exit Function
        If CInt(month) < 1 And CInt(month) > 12 Then Exit Function
        If Len(month) = 1 Then month = "0" + month
        If year = "" Then Exit Function
        If Not IsNumeric(year) Then Exit Function
        If CInt(year) < 1950 Or CInt(year) > 2050 Then Exit Function
        If IsDate(month & "/" & day & "/" & year) Then verifica_fecha_previus = 0
    End Function

    'aqui cambie esteban 12/09/2013
    'Public Function compara_datos_regeneracion() As Boolean
    '    Dim row As Integer
    '    Dim col As Integer
    '    compara_datos_regeneracion = True
    '    For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
    '        For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
    '            With UPDATEMGB.DataGridView1(col, row)
    '                tipo_error = 0
    '                Select Case col
    '                    Case 12, 15
    '                        tipo_error = verifica_fecha(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString, devuelve_year(row))
    '                        If tipo_error > 0 Then
    '                            compara_datos_regeneracion = False
    '                            MARCA_ERROR(tipo_error, col, row, tam_max)
    '                        End If
    '                        col = col + 1

    '                    Case 76, 80, 84, 92, 96, 100
    '                        If .Value.ToString = "" Then
    '                            If Not busca_campos_vacios(col, row) Then
    '                                tipo_error = 4

    '                            End If

    '                        ElseIf Not IsNumeric(.Value.ToString) Then
    '                            tipo_error = 2

    '                        Else
    '                            If busca_campos_vacios(col, row) Then
    '                                tipo_error = 4

    '                            End If
    '                        End If
    '                    Case 73, 77, 81, 85, 89, 93, 97, 101
    '                        tipo_error = verifica_fecha(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString, UPDATEMGB.DataGridView1(col + 2, row).Value.ToString)
    '                        If tipo_error > 0 Then
    '                            compara_datos_regeneracion = False
    '                            MARCA_ERROR(tipo_error, col, row, tam_max)
    '                            MARCA_ERROR(tipo_error, col + 1, row, tam_max)
    '                        End If
    '                        col = col + 2

    '                    Case Else
    '                        If CInt(arr_null(col)) = 1 And .Value.ToString = "" Then
    '                            tipo_error = 1
    '                        ElseIf .Value.ToString <> "" Then
    '                            tipo_error = verifica_tipo_de_dato(.Value.ToString, arr_tipodato(col), arr_maxvalor(col))
    '                            If tipo_error = 0 Then
    '                                If arr_valmin(col) <> "" And arr_valmax(col) <> "" Then
    '                                    tipo_error = verifica_max_min_interno(.Value.ToString, arr_tipodato(col), arr_valmin(col), arr_valmax(col))
    '                                End If
    '                            End If
    '                        End If

    '                End Select
    '                If tipo_error > 0 Then
    '                    compara_datos_regeneracion = False
    '                    MARCA_ERROR(tipo_error, col, row, tam_max)
    '                End If
    '            End With
    '        Next
    '    Next


    'End Function
    'Public Function compara_fe()
    Public Function busca_campos_vacios(ByVal col As Integer, ByVal row As Integer) As Boolean
        busca_campos_vacios = False
        With UPDATEMGB
            If .DataGridView1(col + 1, row).Value.ToString = "" And .DataGridView1(col + 2, row).Value.ToString = "" And .DataGridView1(col + 3, row).Value.ToString = "" Then
                busca_campos_vacios = True
                Exit Function
            End If
            If verifica_fecha(.DataGridView1(col + 1, row).Value.ToString, .DataGridView1(col + 2, row).Value.ToString, .DataGridView1(col + 3, row).Value.ToString) _
                = 0 Then Exit Function
        End With
        busca_campos_vacios = True

    End Function
    Public Function existe_collectionnumber_in_db(ByVal collection1 As String, ByVal collection2 As String) As Boolean
        Dim cmd As New SqlCommand
        existe_collectionnumber_in_db = False
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select COUNT(*) from collection where ltrim(rtrim(collectionnumber1)) ='" & collection1 & "' and ltrim(rtrim(collectionNumber2)) = '" & collection2 & "'"
        If cmd.ExecuteScalar > 0 Then existe_collectionnumber_in_db = True
    End Function

    Public Function existe_id_collectionnumber(ByVal id As String, ByVal row As Integer) As Boolean
        existe_id_collectionnumber = False
        Dim row1 As Integer
        If row = 0 Then Exit Function
        With UPDATEMGB
            For row1 = 0 To row - 1
                If id = (.DataGridView1(38, row1).ToString & .DataGridView1(39, row1).ToString) Then
                    existe_id_collectionnumber = True
                    Exit Function
                End If
            Next
        End With
    End Function
    Public Function verifica_fecha_correcta(ByVal fec As String) As Integer
        verifica_fecha_correcta = 4
        If Len(fec) < 6 Then Exit Function
        If Len(fec) = 6 Then
            verifica_fecha_correcta = verifica_fecha(Mid(fec, 6, 1), Mid(fec, 5, 1), Mid(fec, 1, 4))
            Exit Function
        ElseIf Len(fec) = 7 Then
            verifica_fecha_correcta = verifica_fecha(Mid(fec, 7, 1), Mid(fec, 5, 2), Mid(fec, 1, 4))
            Exit Function
        ElseIf Len(fec) = 8 Then
            verifica_fecha_correcta = verifica_fecha(Mid(fec, 7, 2), Mid(fec, 5, 2), Mid(fec, 1, 4))
            Exit Function
        End If
    End Function
    Public Function verifica_fecha(ByVal day As String, ByVal month As String, ByVal year As String) As Integer
        verifica_fecha = 4
        If day = "" And month = "" And year = "" Then
            verifica_fecha = 0
            Exit Function
        End If
        If day = "" Then Exit Function
        If Not IsNumeric(day) Then Exit Function
        If CInt(day) < 1 And CInt(day) > 31 Then Exit Function
        If Len(day) = 1 Then day = "0" + day
        If month = "" Then Exit Function
        If Not IsNumeric(month) Then Exit Function
        If CInt(month) < 1 And CInt(month) > 12 Then Exit Function
        If Len(month) = 1 Then month = "0" + month
        If year = "" Then Exit Function
        If Not IsNumeric(year) Then Exit Function
        If CInt(year) < 1950 And CInt(year) > 2050 Then Exit Function
        If IsDate(month & "/" & day & "/" & year) Then verifica_fecha = 0
    End Function
    Public Function verifica_primrayrace(ByVal primraryrace As String) As Integer
        verifica_primrayrace = 0
        Dim cmd As SqlCommand
        Dim str As String
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select forma from races where primaryrace = '" & primraryrace & "'"
        str = cmd.ExecuteScalar
        If Len(str) = 0 Then verifica_primrayrace = 6
    End Function
    Public Function valida_fieldbookid(ByVal id As Integer) As Boolean
        valida_fieldbookid = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select usercode from FIELDBOOKS where FieldbookID = " & id
        num = cmd.ExecuteScalar
        If num = 0 Then valida_fieldbookid = True
    End Function
    Public Function devuelve_posible_acc() As Integer
        devuelve_posible_acc = 0
        Dim cmd As New SqlCommand
        Dim str As String
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select top 1 [accid] from acc order by accid desc"
        str = cmd.ExecuteScalar
        If Len(Trim(str)) = 0 Then
            MsgBox("Error AccID")
            Exit Function
        End If
        devuelve_posible_acc = CInt(Mid(str, 10, 15)) + 1
    End Function
    Public Function valida_accid(ByVal id As Integer) As Boolean
        valida_accid = False
        Dim cmd As SqlCommand
        Dim num As String
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        Dim cad As String
        cad = "CIMMYTMA-" & Format(Trim$(id), "000000")
        cmd.CommandText = "Select * from acc where accid = '" & cad & "'"
        num = cmd.ExecuteScalar
        If Len(Trim(num)) = 0 Then valida_accid = True
    End Function
    Public Function verifica_max_min_interno(ByVal dato As String, ByVal tipo As String, ByVal val_min As String, ByVal val_max As String) As Integer
        verifica_max_min_interno = 0

        If val_min = "TOKEN" Then
            If Not existe_en_token(dato, val_max) Then
                verifica_max_min_interno = 2
                Exit Function
            End If
        ElseIf tipo = "varchar" Then
            If Not (dato >= val_min And dato <= val_max) Then
                verifica_max_min_interno = 2
                Exit Function
            End If
        ElseIf Not (Val(dato) >= CInt(val_min) And Val(dato) <= CInt(val_max)) Then
            verifica_max_min_interno = 2
            Exit Function
        End If
    End Function

    Public Function verifica_tipo_de_dato(ByVal dato As String, ByVal tipo As String, ByVal max As Integer) As Byte
        verifica_tipo_de_dato = 0
        If tipo = "varchar" Then
            If Len(dato) > max Then
                verifica_tipo_de_dato = 5
                tam_max = max
                Exit Function
            End If
        ElseIf tipo = "real" Then
            If Not IsNumeric(dato) Then
                verifica_tipo_de_dato = 2
                Exit Function
            End If
        ElseIf tipo = "numero" Then

            If Val(dato) > max Then
                verifica_tipo_de_dato = 4
                Exit Function
            End If
        Else
            If (Val(dato) Mod 1) <> 0 Then
                verifica_tipo_de_dato = 2
                Exit Function
            End If
            If Val(dato) > Val(devuelve_valorMax_permitido(tipo)) Then
                verifica_tipo_de_dato = 5
                Exit Function
            End If
        End If
    End Function
    Public Function devuelve_valorMax_permitido(ByVal tipo As String) As String
        devuelve_valorMax_permitido = ""
        Select Case tipo
            Case "int"
                devuelve_valorMax_permitido = "2147483647"
            Case "smallint"
                devuelve_valorMax_permitido = "32767"
            Case "tinyint"
                devuelve_valorMax_permitido = "255"
        End Select
    End Function
    Public Function GUARDA_EN_COOPMGB() As Boolean
        GUARDA_EN_COOPMGB = False
        Dim row As Integer
        Dim col As Integer
        Dim STR As String
        Dim CMD As New SqlCommand
        Dim datare As SqlDataReader
        Dim dtt As System.Data.DataTable
        Try
            CMD.Connection = cnx
            CMD.CommandType = CommandType.Text
            For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
                If UPDATEMGB.DataGridView1(14, row).Value.ToString = "I" Then
                    dtt = New System.Data.DataTable
                    STR = "SELECT Address1, Address2, Address3, Address4, Address5, Address6, Phone, Fax, Telex, EmailAddress FROM INSTITUTION WHERE INSTITUTIONID = " & CInt(UPDATEMGB.DataGridView1(6, row).Value.ToString)
                    CMD.CommandText = STR
                    datare = CMD.ExecuteReader
                    dtt.Load(datare)
                    STR = "INSERT INTO COOPMGB VALUES ("
                    For col = 15 To 25
                        With UPDATEMGB.DataGridView1(col, row)
                            If col = 15 Then
                                STR = STR & CInt(.Value.ToString) & ",'" & UPDATEMGB.DataGridView1(0, row).Value.ToString & "',"
                            ElseIf col > 15 And col < 24 Then
                                STR = STR & "'" & dtt.Rows.Item(col - 16).ToString & "',"
                            ElseIf col = 25 Then
                                STR = STR & "'" & dtt.Rows.Item(col - 16).ToString & "')"
                            End If
                        End With
                    Next
                Else
                    STR = "INSERT INTO COOPMGB VALUES ("
                    For col = 15 To 25
                        With UPDATEMGB.DataGridView1(col, row)
                            If col = 15 Then
                                STR = STR & CInt(.Value.ToString) & ",'" & UPDATEMGB.DataGridView1(0, row).Value.ToString & "'," & CInt(UPDATEMGB.DataGridView1(6, row).Value.ToString) & ","
                            ElseIf col > 15 And col <= 24 Then
                                STR = STR & "'" & .Value.ToString & "',"
                            ElseIf col = 25 Then
                                STR = STR & "'" & .Value.ToString & "')"
                            End If
                        End With
                    Next
                End If
                CMD.CommandText = STR
                CMD.Transaction = tran
                CMD.ExecuteNonQuery()
            Next
            GUARDA_EN_COOPMGB = True
        Catch ex As Exception
            MsgBox(ex.Message)
            GUARDA_EN_COOPMGB = False
        End Try
    End Function
    Public Function GUARDA_EN_COOP() As Boolean
        Dim row As Integer
        Dim col As Integer
        Dim STR As String
        Dim CMD As New SqlCommand
        Dim datare As SqlDataReader
        Dim dtt As System.Data.DataTable
        GUARDA_EN_COOP = False
        Try
            CMD.Connection = cnx
            CMD.CommandType = CommandType.Text
            For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
                If UPDATEMGB.DataGridView1(7, row).Value.ToString = "I" Then
                    dtt = New System.Data.DataTable
                    STR = "SELECT Address1, Address2, Address3, Address4, Address5, Address6 FROM INSTITUTION WHERE INSTITUTIONID = " & CInt(UPDATEMGB.DataGridView1(6, row).Value.ToString)
                    CMD.CommandText = STR
                    datare = CMD.ExecuteReader
                    dtt.Load(datare)
                    STR = "INSERT INTO COOP VALUES ("
                    For col = 0 To 13
                        With UPDATEMGB.DataGridView1(col, row)
                            If arr_tipodato(col) = "varchar" And col > 7 Then
                                STR = STR & "'" & .Value.ToString & "',"
                            ElseIf arr_tipodato(col) = "int" And col > 7 Then
                                STR = STR & CInt(.Value.ToString) & ","
                            ElseIf col > 7 And col < 13 Then
                                STR = STR & "'" & dtt.Rows.Item(col - 8).ToString & "',"
                            ElseIf col = 13 Then
                                STR = STR & "'" & dtt.Rows.Item(col - 8).ToString & "')"
                            End If
                        End With
                    Next
                Else
                    STR = "INSERT INTO COOP VALUES ("
                    For col = 0 To 13
                        With UPDATEMGB.DataGridView1(col, row)
                            If arr_tipodato(col) = "varchar" And col <> 6 And col <> 7 And col <> 13 Then
                                STR = STR & "'" & .Value.ToString & "',"
                            ElseIf arr_tipodato(col) = "int" And col <> 6 And col <> 7 And col <> 13 Then
                                STR = STR & CInt(.Value.ToString) & ","
                            ElseIf col = 13 Then
                                STR = STR & "'" & .Value.ToString & "')"
                            End If
                        End With
                    Next
                End If
                CMD.CommandText = STR
                CMD.Transaction = tran
                CMD.ExecuteNonQuery()
            Next
            GUARDA_EN_COOP = True
        Catch ex As Exception
            MsgBox(ex.Message)
            GUARDA_EN_COOP = False
        End Try
    End Function
    Public Function guarda_accid() As Boolean
        Dim str As String
        Dim source As String
        'Dim accid As Integer
        Dim accid_completo As String
        guarda_accid = False
        NUM_OP = 3 * (UPDATEMGB.DataGridView1.RowCount - 2)
        'UPDATEMGB.PGRBAR.Visible = True
        'UPDATEMGB.PGRBAR.Minimum = 1
        'UPDATEMGB.PGRBAR.Maximum = NUM_OP
        'UPDATEMGB.PGRBAR.Value = 1
        'UPDATEMGB.PGRBAR.Step = 1
        If Not cnx.State = ConnectionState.Open Then Exit Function
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            Dim cmd = New SqlCommand("addAcc", cnx)
            cmd.CommandType = CommandType.StoredProcedure
            Dim cmd1 = New SqlCommand("PLongBookAcc", cnx)
            cmd1.CommandType = CommandType.StoredProcedure
            'cambie esteban 12/09/2013 para accid
            'accid_completo = "CIMMYTMA-" + llena_ceros(CStr(devuelve_posible_acc()))
            accid_completo = UPDATEMGB.DataGridView1(0, row).Value
            cmd.Parameters.Add("@p_Accid", SqlDbType.VarChar, 50)
            cmd.Parameters("@p_Accid").Value = accid_completo
            cmd1.Parameters.Add("@p_AccId", SqlDbType.VarChar, 50)
            cmd1.Parameters("@p_AccId").Value = accid_completo
            cmd1.Parameters.Add("@p_Action", SqlDbType.VarChar, 10)
            cmd1.Parameters("@p_Action").Value = "Add"
            cmd1.Parameters.Add("@p_NameUser", SqlDbType.VarChar, 50)
            cmd1.Parameters("@p_NameUser").Value = user
            cmd1.Parameters.Add("@p_DateChange", SqlDbType.VarChar, 20)
            cmd1.Parameters("@p_DateChange").Value = Now
            'UPDATEMGB.DataGridView1(0, row).Value = accid_completo
            cmd.Parameters.Add("@p_Collectno", SqlDbType.VarChar, 50)
            cmd.Parameters("@p_Collectno").Value = Trim(UPDATEMGB.DataGridView1(38, row).Value.ToString) & " " & _
                                                   Trim(UPDATEMGB.DataGridView1(39, row).Value.ToString)
            cmd.Parameters.Add("@p_Taxno_ref", SqlDbType.VarChar, 7)
            cmd.Parameters("@p_Taxno_ref").Value = Trim(UPDATEMGB.DataGridView1(6, row).Value.ToString)
            cmd.Parameters.Add("@p_Stat", SqlDbType.VarChar, 30)
            cmd.Parameters("@p_Stat").Value = Trim(UPDATEMGB.DataGridView1(21, row).Value.ToString)
            Select Case Trim(UPDATEMGB.DataGridView1(21, row).Value.ToString)
                Case "1"
                    source = "2"
                Case "7", "8"
                    source = "1"
                Case Else
                    source = "7"
            End Select
            cmd.Parameters.Add("@p_Source", SqlDbType.VarChar, 4)
            cmd.Parameters("@p_Source").Value = source
            cmd.Parameters.Add("@p_Datecoll", SqlDbType.VarChar, 8)
            cmd.Parameters("@p_Datecoll").Value = Trim(UPDATEMGB.DataGridView1(41, row).Value.ToString)
            For col = 63 To UPDATEMGB.DataGridView1.ColumnCount - 1
                With UPDATEMGB.DataGridView1(col, row)
                    str = "@p_" & arr_nombrebds(col)
                    cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                    cmd.Parameters(str).Value = .Value.ToString
                End With
            Next col
            cmd.Transaction = tran
            cmd.ExecuteNonQuery()
            cmd1.Transaction = tran
            cmd1.ExecuteNonQuery()
            UPDATEMGB.PGRBAR.PerformStep()
        Next row
        guarda_accid = True
    End Function
    Public Function guarda_passport() As Boolean
        Dim str As String
        guarda_passport = False
        If Not cnx.State = ConnectionState.Open Then Exit Function
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            Dim cmd = New SqlCommand("updatePassport", cnx)
            cmd.CommandType = CommandType.StoredProcedure
            Dim cmd1 = New SqlCommand("PLongBookPassport", cnx)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.Add("@p_AccId", SqlDbType.VarChar, 50)
            'aqui cambie esteban 12/09/2013
            'cmd1.Parameters("@p_AccId").Value = "CIMMYTMA-" + llena_ceros(Trim(UPDATEMGB.DataGridView1(0, row).Value.ToString))
            cmd1.Parameters("@p_AccId").Value = UPDATEMGB.DataGridView1(0, row).Value
            cmd1.Parameters.Add("@p_Action", SqlDbType.VarChar, 10)
            cmd1.Parameters("@p_Action").Value = "Add"
            cmd1.Parameters.Add("@p_NameUser", SqlDbType.VarChar, 50)
            cmd1.Parameters("@p_NameUser").Value = user
            cmd1.Parameters.Add("@p_DateChange", SqlDbType.VarChar, 20)
            cmd1.Parameters("@p_DateChange").Value = Now
            For col = 0 To 36
                With (UPDATEMGB.DataGridView1(col, row))
                    str = "@p_" & arr_nombrebds(col)
                    cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                    cmd.Parameters(str).Value = .Value.ToString
                End With
            Next col
            cmd.Transaction = tran
            cmd.ExecuteNonQuery()
            cmd1.Transaction = tran
            cmd1.ExecuteNonQuery()
            UPDATEMGB.PGRBAR.PerformStep()
        Next row
        guarda_passport = True
    End Function
    Public Function guarda_collection() As Boolean
        guarda_collection = False
        Dim str As String
        If Not cnx.State = ConnectionState.Open Then Exit Function
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            Dim cmd1 = New SqlCommand("plongbookcollection", cnx)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.Add("@p_AccId", SqlDbType.VarChar, 50)
            'aqui cambie esteban 12/09/2013
            'cmd1.Parameters("@p_AccId").Value = "CIMMYTMA-" + llena_ceros(Trim(UPDATEMGB.DataGridView1(0, row).Value.ToString))
            cmd1.Parameters("@p_AccId").Value = UPDATEMGB.DataGridView1(0, row).Value
            cmd1.Parameters.Add("@p_Action", SqlDbType.VarChar, 10)
            cmd1.Parameters("@p_Action").Value = "Add"
            cmd1.Parameters.Add("@p_NameUser", SqlDbType.VarChar, 50)
            cmd1.Parameters("@p_NameUser").Value = user
            cmd1.Parameters.Add("@p_DateChange", SqlDbType.VarChar, 20)
            cmd1.Parameters("@p_DateChange").Value = Now
            Dim cmd = New SqlCommand("updateCollection", cnx)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@p_AccId", SqlDbType.VarChar, 50)
            'aqui cambie esteban 12/09/2013
            'cmd.Parameters("@p_AccId").Value = "CIMMYTMA-" + llena_ceros(Trim(UPDATEMGB.DataGridView1(0, row).Value.ToString))
            cmd.Parameters("@p_AccId").Value = UPDATEMGB.DataGridView1(0, row).Value
            cmd.Parameters.Add("@p_LocalName", SqlDbType.VarChar, 50)
            cmd.Parameters("@p_LocalName").Value = UPDATEMGB.DataGridView1(2, row).Value.ToString
            For col = 37 To 62
                With (UPDATEMGB.DataGridView1(col, row))
                    str = "@p_" & arr_nombrebds(col)
                    cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                    cmd.Parameters(str).Value = .Value.ToString
                End With
            Next col
            cmd.Transaction = tran
            cmd.ExecuteNonQuery()
            cmd1.Transaction = tran
            cmd1.ExecuteNonQuery()
            UPDATEMGB.PGRBAR.PerformStep()
        Next row
        guarda_collection = True
    End Function
    Public Function crea_excel_regen() As Boolean
        Dim excel As New Microsoft.Office.Interop.Excel.ApplicationClass
        Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim col As Integer
        Dim row As Integer
        Dim fechas As String

        Dim cmd As New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        Dim accid As String
        crea_excel_regen = False
        wBook = excel.Workbooks.Add
        wSheet = wBook.ActiveSheet
        For col = 0 To UPDATEMGB.DataGridView1.ColumnCount
            If col = 0 Then
                excel.Cells(1, col + 1) = arr_encabezado(col)
            ElseIf col = 1 Then
                excel.Cells(1, col + 1) = "BankAccessionNumber"
            Else
                excel.Cells(1, col + 1) = arr_encabezado(col - 1)
            End If
        Next


        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            'aqui cambie 12/09/2013
            'accid = "CIMMYTMA-" + llena_ceros(Trim(UPDATEMGB.DataGridView1(0, row).Value.ToString))
            accid = UPDATEMGB.DataGridView1(0, row).Value.ToString
            cmd.CommandText = "select BankAccessionNumber from passport where accid ='" & accid & "'"
            cmd.Transaction = tran
            For col = 0 To UPDATEMGB.DataGridView1.ColumnCount
                If col = 0 Then
                    excel.Cells(row + 2, col + 1) = UPDATEMGB.DataGridView1(col, row).Value.ToString
                ElseIf col = 1 Then
                    excel.Cells(row + 2, col + 1) = cmd.ExecuteScalar
                Else
                    excel.Cells(row + 2, col + 1) = UPDATEMGB.DataGridView1(col - 1, row).Value.ToString
                End If
            Next col
        Next row
        fechas = Now
        fechas = Trim(fechas)
        fechas = Replace(fechas, "/", "_")
        fechas = Replace(fechas, ":", "_")
        wSheet.Columns.AutoFit()
        wBook.SaveAs(path_aplicacion & "REGEN_" & fechas & ".xlsx")
        excel.Workbooks.Close()
        crea_excel_regen = True
    End Function

    Public Function crea_excel_passport() As Boolean
        Dim excel As New Microsoft.Office.Interop.Excel.ApplicationClass
        Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim col As Integer
        Dim row As Integer
        Dim fechas As String
        crea_excel_passport = False
        wBook = excel.Workbooks.Add
        wSheet = wBook.ActiveSheet
        For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
            excel.Cells(1, col + 1) = arr_encabezado(col)
        Next
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            For col = 0 To UPDATEMGB.DataGridView1.ColumnCount - 1
                excel.Cells(row + 2, col + 1) = UPDATEMGB.DataGridView1(col, row).Value.ToString
            Next col
        Next row
        fechas = Now
        fechas = Trim(fechas)
        fechas = Replace(fechas, "/", "_")
        fechas = Replace(fechas, ":", "_")
        wSheet.Columns.AutoFit()
        wBook.SaveAs(path_aplicacion & "PASSPORT_" & fechas & ".xlsx")
        excel.Workbooks.Close()
        crea_excel_passport = True
    End Function

    Public Sub guarda_actbas_cmd(ByVal act As Byte, ByVal ini As Integer, ByVal fin As Integer, ByVal row As Integer)

        Dim str As String
        Dim val As Integer
        If Not cnx.State = ConnectionState.Open Then Exit Sub
        Dim cmd = New SqlCommand("AddActBasCollectionFromRegenerationnet", cnx)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@p_AccId", SqlDbType.VarChar, 50)
        cmd.Parameters("@p_AccId").Value = "CIMMYTMA-" + llena_ceros(Trim(UPDATEMGB.DataGridView1(0, row).Value.ToString))
        cmd.Parameters.Add("@p_Active", SqlDbType.TinyInt)
        cmd.Parameters("@p_Active").Value = act
        cmd.Parameters.Add("@p_acc_status", SqlDbType.Char, 1)
        cmd.Parameters("@p_acc_status").Value = UPDATEMGB.DataGridView1(1, row).Value.ToString
        cmd.Parameters.Add("@p_RegenerationBasePackages", SqlDbType.SmallInt)
        If UPDATEMGB.DataGridView1(106, row).Value.ToString = "" Then
            cmd.Parameters("@p_RegenerationBasePackages").Value = 0
        Else
            cmd.Parameters("@p_RegenerationBasePackages").Value = UPDATEMGB.DataGridView1(106, row).Value.ToString
        End If

        For col = ini To fin
            With UPDATEMGB.DataGridView1(col, row)
                If col = 106 Then
                    MsgBox("aqui")
                End If
                str = "@p_" & arr_nombrebds(col)
                Select Case col
                    Case 75, 79, 83, 87, 91, 95, 99, 103
                        cmd.Parameters.Add(str, devuelte_tipo_de_dato(arr_tipodato(col)), arr_maxpro(col))
                        cmd.Parameters(str).Value = ARMA_FECHA_ALT(Trim(.Value.ToString), Trim(UPDATEMGB.DataGridView1(col + 1, row).Value.ToString), Trim(UPDATEMGB.DataGridView1(col + 2, row).Value.ToString))
                        col = col + 2
                    Case Else
                        cmd.Parameters.Add(str, devuelte_tipo_de_dato(arr_tipodato(col)))
                        If .Value.ToString = "" Then
                            cmd.Parameters(str).Value = 0
                        Else
                            val = .Value.ToString
                            cmd.Parameters(str).Value = val
                        End If
                End Select
            End With
        Next col
        Try
            cmd.Transaction = tran
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Public Function guarda_actbascollection() As Boolean
        Dim ini As Integer
        Dim fin As Integer
        Dim act As Byte
        guarda_actbascollection = False
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            If UPDATEMGB.DataGridView1(73, row).Value.ToString = "A" Or _
               UPDATEMGB.DataGridView1(73, row).Value.ToString = "X" Then
                act = 0
                ini = 74
                fin = 89
                guarda_actbas_cmd(act, ini, fin, row)
            ElseIf UPDATEMGB.DataGridView1(73, row).Value.ToString = "B" Then
                act = 1
                ini = 90
                fin = 105
                guarda_actbas_cmd(act, ini, fin, row)
            ElseIf UPDATEMGB.DataGridView1(73, row).Value.ToString = "Z" Then
                act = 0
                ini = 74
                fin = 89
                guarda_actbas_cmd(act, ini, fin, row)
                act = 1
                ini = 90
                fin = 105
                guarda_actbas_cmd(act, ini, fin, row)
            End If

            UPDATEMGB.PGRBAR.PerformStep()
        Next row
        guarda_actbascollection = True
    End Function
    Public Function guarda_regeneracion() As Boolean
        Dim str As String
        Dim year As String
        Dim res As Integer
        guarda_regeneracion = False
        If cnx.State = ConnectionState.Closed Then Exit Function
        NUM_OP = 2 * (UPDATEMGB.DataGridView1.RowCount - 2)
        UPDATEMGB.PGRBAR.Visible = True
        UPDATEMGB.PGRBAR.Minimum = 1
        UPDATEMGB.PGRBAR.Maximum = NUM_OP
        UPDATEMGB.PGRBAR.Value = 1
        UPDATEMGB.PGRBAR.Step = 1
        For row = 0 To UPDATEMGB.DataGridView1.RowCount - 2
            If UPDATEMGB.DataGridView1(73, row).Value.ToString = "X" Then
                str = "AddUpdateRegenerationnet_alterno"
            Else
                str = "AddUpdateRegenerationnet"
            End If

            Dim cmd As New SqlCommand(str, cnx)
            cmd.CommandType = CommandType.StoredProcedure
            str = ""
            For col = 0 To 72
                With UPDATEMGB.DataGridView1(col, row)
                    str = "@p_" & arr_nombrebds(col)
                    Select Case col
                        Case 0
                            cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                            If .Value.ToString = "" Then
                                cmd.Parameters(str).Value = ""
                            Else
                                cmd.Parameters(str).Value = "CIMMYTMA-" + llena_ceros(Trim(.Value.ToString))
                            End If
                        Case 12, 16
                            cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                            cmd.Parameters(str).Value = ARMA_FECHA_ALT(Trim(.Value.ToString), Trim(UPDATEMGB.DataGridView1(col + 1, row).Value.ToString), Trim(UPDATEMGB.DataGridView1(col + 2, row).Value.ToString))
                            col = col + 2

                        Case 60

                        Case 69
                            cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                            cmd.Parameters(str).Value = ARMA_FECHA(.Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString, UPDATEMGB.DataGridView1(col + 1, row).Value.ToString)
                            col = col + 2
                        Case Else
                            cmd.Parameters.Add(str, SqlDbType.VarChar, arr_maxpro(col))
                            cmd.Parameters(str).Value = .Value.ToString
                    End Select
                End With
            Next col
            Try
                cmd.Transaction = tran
                cmd.ExecuteNonQuery()
                UPDATEMGB.PGRBAR.PerformStep()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next row
        guarda_regeneracion = True
    End Function
    Public Function devuelve_year(ByVal row As Integer) As String
        Dim year As String
        devuelve_year = ""
        With UPDATEMGB.DataGridView1(3, row)
            If .Value.ToString = "" Then
                year = "XXXX"
            ElseIf IsNumeric(Mid(.Value.ToString, 3, 2)) Then
                year = Mid(.Value.ToString, 3, 2)
            ElseIf IsNumeric(Mid(.Value.ToString, 4, 2)) Then
                year = Mid(.Value.ToString, 4, 2)
            Else
                year = "XXXX"
            End If
        End With
    End Function
    Public Function devuelte_tipo_de_dato(ByVal str As String) As System.Data.SqlDbType
        Select Case str
            Case "varchar"
                devuelte_tipo_de_dato = SqlDbType.VarChar
            Case "smallint"
                devuelte_tipo_de_dato = SqlDbType.SmallInt
            Case "tinyint"
                devuelte_tipo_de_dato = SqlDbType.TinyInt
            Case "int"
                devuelte_tipo_de_dato = SqlDbType.Int
            Case "numero"
                devuelte_tipo_de_dato = SqlDbType.VarChar
        End Select
    End Function
    Public Function ARMA_FECHA_ALT(ByVal DAY As String, ByVal MONTH As String, ByVal YEAR As String) As String
        ARMA_FECHA_ALT = ""
        If YEAR = "" Or DAY = "" Or MONTH = "" Then Exit Function
        If YEAR = "XXXX" Then
            Exit Function
        End If
        If DAY = "" Then
            DAY = "00"
        ElseIf Len(DAY) = 1 Then
            DAY = "0" + DAY
        End If
        If MONTH = "" Then
            MONTH = "00"
        ElseIf Len(MONTH) = 1 Then
            MONTH = "0" + MONTH
        End If
        ARMA_FECHA_ALT = YEAR + MONTH + DAY
    End Function
    Public Function ARMA_FECHA(ByVal DAY As String, ByVal MONTH As String, ByVal YEAR As String) As String
        ARMA_FECHA = ""
        If YEAR = "" Or DAY = "" Or MONTH = "" Then Exit Function
        If YEAR = "XXXX" Then
            Exit Function
        End If
        If DAY = "" Then
            DAY = "00"
        ElseIf Len(DAY) = 1 Then
            DAY = "0" + DAY
        End If
        If MONTH = "" Then
            MONTH = "00"
        ElseIf Len(MONTH) = 1 Then
            MONTH = "0" + MONTH
        End If
        ARMA_FECHA = YEAR + MONTH + DAY
    End Function
    Public Function existe_coopmgb(ByVal cooperatorid As Integer) As Boolean
        existe_coopmgb = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select count(*) from coopmgb where CooperatorID = '" & cooperatorid & "'"
        num = cmd.ExecuteScalar
        If num > 0 Then existe_coopmgb = True
    End Function
    Public Function existe_institution(ByVal idinstitution As Integer) As Boolean
        existe_institution = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select count(*) from INSTITUTION where InstitutionID = '" & idinstitution & "'"
        num = cmd.ExecuteScalar
        If num > 0 Then existe_institution = True
    End Function
    Public Function existe_ccode(ByVal coop As String) As Boolean
        existe_ccode = False
        Dim cmd As SqlCommand
        Dim num As Integer
        num = 0
        cmd = New SqlCommand
        cmd.Connection = cnx
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select count(*) from coop where ccode = '" & coop & "'"
        num = cmd.ExecuteScalar
        If num > 0 Then existe_ccode = True

    End Function
    Public Function llena_ceros(ByVal Str As String) As String
        Dim i As Byte
        llena_ceros = ""
        For i = 1 To (6 - Len(Str))
            Str = "0" + Str
        Next
        llena_ceros = Str
    End Function
End Module
